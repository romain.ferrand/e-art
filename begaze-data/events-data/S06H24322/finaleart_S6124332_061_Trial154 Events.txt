[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S6124332-finaleart-1.idf
Date:	27.03.2019 15:36:03
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S6124332
Description:	Romain Ferrand

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	12110595874	# Message: void.jpg
Fixation L	1	1	12110600142	12110818873	218731	856.96	685.04	64	34	-1	19.39	19.39
Fixation R	1	1	12110600142	12110818873	218731	856.96	685.04	64	34	-1	19.95	19.95
Saccade L	1	1	12110818873	12110838870	19997	885.18	695.63	886.55	696.35	0.08	8.40	1.00	3.89	39.88	-170.75	83.49
Saccade R	1	1	12110818873	12110838870	19997	885.18	695.63	886.55	696.35	0.08	8.40	1.00	3.89	39.88	-170.75	83.49
Fixation L	1	2	12110838870	12112589146	1750276	897.07	718.57	22	62	-1	19.87	19.87
Fixation R	1	2	12110838870	12112589146	1750276	897.07	718.57	22	62	-1	19.96	19.96
