[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S6124332-finaleart-1.idf
Date:	27.03.2019 15:36:15
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S6124332
Description:	Romain Ferrand

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	12562854341	# Message: void.jpg
Fixation L	1	1	12562854875	12563769825	914950	1174.47	593.30	7	41	-1	21.69	21.69
Fixation R	1	1	12562854875	12563769825	914950	1174.47	593.30	7	41	-1	21.28	21.28
Saccade L	1	1	12563769825	12563809559	39734	1172.90	616.52	963.53	625.62	1.95	137.03	1.00	49.12	3362.13	-3112.71	2378.86
Saccade R	1	1	12563769825	12563809559	39734	1172.90	616.52	963.53	625.62	1.95	137.03	1.00	49.12	3362.13	-3112.71	2378.86
Fixation L	1	2	12563809559	12564227289	417730	921.07	655.60	50	48	-1	22.38	22.38
Fixation R	1	2	12563809559	12564227289	417730	921.07	655.60	50	48	-1	21.10	21.10
Saccade L	1	2	12564227289	12564247153	19864	914.64	673.78	915.01	677.43	0.04	3.71	1.00	1.80	79.41	-45.84	58.76
Saccade R	1	2	12564227289	12564247153	19864	914.64	673.78	915.01	677.43	0.04	3.71	1.00	1.80	79.41	-45.84	58.76
Fixation L	1	3	12564247153	12564446019	198866	926.03	682.70	73	16	-1	22.51	22.51
Fixation R	1	3	12564247153	12564446019	198866	926.03	682.70	73	16	-1	21.28	21.28
Saccade L	1	3	12564446019	12564465900	19881	985.74	674.28	992.28	655.95	0.53	62.47	1.00	26.46	159.84	-1069.37	445.01
Saccade R	1	3	12564446019	12564465900	19881	985.74	674.28	992.28	655.95	0.53	62.47	1.00	26.46	159.84	-1069.37	445.01
Fixation L	1	4	12564465900	12564863747	397847	1009.18	617.83	24	47	-1	21.85	21.85
Fixation R	1	4	12564465900	12564863747	397847	1009.18	617.83	24	47	-1	20.41	20.41
