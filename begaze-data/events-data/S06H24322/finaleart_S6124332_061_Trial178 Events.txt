[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S6124332-finaleart-1.idf
Date:	27.03.2019 15:36:06
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S6124332
Description:	Romain Ferrand

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	12347257742	# Message: void.jpg
Fixation L	1	1	12347257877	12347377240	119363	1544.90	225.89	14	12	-1	16.56	16.56
Fixation R	1	1	12347257877	12347377240	119363	1544.90	225.89	14	12	-1	15.30	15.30
Saccade L	1	1	12347377240	12347476740	99500	1537.04	230.87	821.46	588.61	11.02	207.56	0.40	110.79	5141.08	-3916.23	2615.95
Saccade R	1	1	12347377240	12347476740	99500	1537.04	230.87	821.46	588.61	11.02	207.56	0.40	110.79	5141.08	-3916.23	2615.95
Fixation L	1	2	12347476740	12347695467	218727	773.93	577.49	55	37	-1	18.27	18.27
Fixation R	1	2	12347476740	12347695467	218727	773.93	577.49	55	37	-1	16.34	16.34
Saccade L	1	2	12347695467	12347715341	19874	765.68	563.07	763.08	554.21	0.08	9.35	1.00	3.79	200.73	-161.71	128.51
Saccade R	1	2	12347695467	12347715341	19874	765.68	563.07	763.08	554.21	0.08	9.35	1.00	3.79	200.73	-161.71	128.51
Fixation L	1	3	12347715341	12348869023	1153682	757.48	571.57	11	48	-1	18.78	18.78
Fixation R	1	3	12347715341	12348869023	1153682	757.48	571.57	11	48	-1	16.92	16.92
Saccade L	1	3	12348869023	12348888898	19875	760.49	585.71	803.64	592.78	0.42	44.23	1.00	21.26	959.61	-989.99	832.35
Saccade R	1	3	12348869023	12348888898	19875	760.49	585.71	803.64	592.78	0.42	44.23	1.00	21.26	959.61	-989.99	832.35
Fixation L	1	4	12348888898	12349246879	357981	836.40	589.51	43	26	-1	19.22	19.22
Fixation R	1	4	12348888898	12349246879	357981	836.40	589.51	43	26	-1	17.77	17.77
