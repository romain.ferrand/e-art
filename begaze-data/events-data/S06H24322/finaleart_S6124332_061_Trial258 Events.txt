[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S6124332-finaleart-1.idf
Date:	27.03.2019 15:36:20
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S6124332
Description:	Romain Ferrand

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	12642905647	# Message: void.jpg
Fixation L	1	1	12642911168	12643408390	497222	1454.90	396.55	28	30	-1	18.26	18.26
Fixation R	1	1	12642911168	12643408390	497222	1454.90	396.55	28	30	-1	17.29	17.29
Blink L	1	1	12643408390	12643547628	139238
Blink R	1	1	12643408390	12643865876	457486
Blink L	1	2	12643567501	12643865876	298375
Fixation L	1	2	12643865876	12644482441	616565	958.67	471.38	29	52	-1	18.59	18.59
Fixation R	1	2	12643865876	12644482441	616565	958.67	471.38	29	52	-1	16.87	16.87
Saccade L	1	1	12644482441	12644502331	19890	981.59	466.70	1014.55	472.22	0.38	33.80	1.00	18.93	780.96	-521.74	480.84
Saccade R	1	1	12644482441	12644502331	19890	981.59	466.70	1014.55	472.22	0.38	33.80	1.00	18.93	780.96	-521.74	480.84
Fixation L	1	3	12644502331	12644900175	397844	1041.81	486.28	45	52	-1	17.91	17.91
Fixation R	1	3	12644502331	12644900175	397844	1041.81	486.28	45	52	-1	14.88	14.88
