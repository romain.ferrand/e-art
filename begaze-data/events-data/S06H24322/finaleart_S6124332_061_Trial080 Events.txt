[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S6124332-finaleart-1.idf
Date:	27.03.2019 15:35:50
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S6124332
Description:	Romain Ferrand

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	11777497765	# Message: void.jpg
Fixation L	1	1	11777500751	11777659999	159248	1175.29	612.18	13	30	-1	19.83	19.83
Fixation R	1	1	11777500751	11777659999	159248	1175.29	612.18	13	30	-1	18.93	18.93
Saccade L	1	1	11777659999	11777719466	59467	1176.17	613.24	787.80	645.00	4.15	148.24	0.67	69.75	3633.97	-3263.65	2720.10
Saccade R	1	1	11777659999	11777719466	59467	1176.17	613.24	787.80	645.00	4.15	148.24	0.67	69.75	3633.97	-3263.65	2720.10
Fixation L	1	2	11777719466	11779072016	1352550	792.10	673.54	30	41	-1	19.22	19.22
Fixation R	1	2	11777719466	11779072016	1352550	792.10	673.54	30	41	-1	18.29	18.29
Saccade L	1	2	11779072016	11779091880	19864	810.47	686.95	814.42	753.15	0.58	67.09	1.00	29.35	1607.89	-1361.80	992.62
Saccade R	1	2	11779072016	11779091880	19864	810.47	686.95	814.42	753.15	0.58	67.09	1.00	29.35	1607.89	-1361.80	992.62
Fixation L	1	3	11779091880	11779529470	437590	844.06	743.30	39	22	-1	18.78	18.78
Fixation R	1	3	11779091880	11779529470	437590	844.06	743.30	39	22	-1	17.95	17.95
