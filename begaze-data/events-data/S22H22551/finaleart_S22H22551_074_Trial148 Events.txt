[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S22H22551-finaleart-1.idf
Date:	27.03.2019 15:57:50
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S22H22551
Description:	Mathieu Chambre

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	5209056225	# Message: void.jpg
Saccade L	1	1	5209068261	5209167625	99364	1306.95	685.58	1051.37	536.20	4.77	192.20	1.00	47.98	4626.97	-4683.45	2232.28
Saccade R	1	1	5209068261	5209167625	99364	1306.95	685.58	1051.37	536.20	4.77	192.20	1.00	47.98	4626.97	-4683.45	2232.28
Fixation L	1	1	5209167625	5209923458	755833	1051.67	526.61	36	60	-1	16.33	16.33
Fixation R	1	1	5209167625	5209923458	755833	1051.67	526.61	36	60	-1	16.35	16.35
Saccade L	1	2	5209923458	5209963213	39755	1069.53	494.16	1073.05	491.49	0.31	24.21	1.00	7.78	29686.98	-152.26	7601.97
Saccade R	1	2	5209923458	5209963213	39755	1069.53	494.16	1073.05	491.49	0.31	24.21	1.00	7.78	29686.98	-152.26	7601.97
Blink L	1	1	5209963213	5210241823	278610
Blink R	1	1	5209963213	5210241823	278610
Fixation L	1	2	5210241823	5211037409	795586	1074.30	442.62	42	35	-1	17.67	17.67
Fixation R	1	2	5210241823	5211037409	795586	1074.30	442.62	42	35	-1	17.34	17.34
