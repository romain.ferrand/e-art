[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S22H22551-finaleart-1.idf
Date:	27.03.2019 15:57:01
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S22H22551
Description:	Mathieu Chambre

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	4797519548	# Message: void.jpg
Fixation L	1	1	4797526935	4798004294	477359	1214.26	382.31	14	16	-1	21.97	21.97
Fixation R	1	1	4797526935	4798004294	477359	1214.26	382.31	14	16	-1	22.79	22.79
Saccade L	1	1	4798004294	4798063905	59611	1221.14	373.23	1098.19	414.25	2.01	97.49	0.33	33.76	29575.87	-2204.09	7147.98
Saccade R	1	1	4798004294	4798063905	59611	1221.14	373.23	1098.19	414.25	2.01	97.49	0.33	33.76	29575.87	-2204.09	7147.98
Blink L	1	1	4798063905	4798322526	258621
Blink R	1	1	4798063905	4798322526	258621
Fixation L	1	2	4798322526	4798819749	497223	989.03	478.79	50	44	-1	16.65	16.65
Fixation R	1	2	4798322526	4798819749	497223	989.03	478.79	50	44	-1	17.05	17.05
Saccade L	1	2	4798819749	4798839624	19875	1020.46	451.95	1022.72	442.86	0.13	9.47	1.00	6.58	162.60	-99.89	112.63
Saccade R	1	2	4798819749	4798839624	19875	1020.46	451.95	1022.72	442.86	0.13	9.47	1.00	6.58	162.60	-99.89	112.63
Fixation L	1	3	4798839624	4799515958	676334	1031.96	445.79	29	45	-1	16.25	16.25
Fixation R	1	3	4798839624	4799515958	676334	1031.96	445.79	29	45	-1	16.56	16.56
