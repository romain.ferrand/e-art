[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S22H22551-finaleart-1.idf
Date:	27.03.2019 15:58:56
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S22H22551
Description:	Mathieu Chambre

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	5800360945	# Message: void.jpg
Saccade L	1	1	5800380500	5800519743	139243	1491.46	245.70	1603.59	263.04	2.42	115.07	0.71	17.39	2841.75	-2681.96	712.08
Saccade R	1	1	5800380500	5800519743	139243	1491.46	245.70	1603.59	263.04	2.42	115.07	0.71	17.39	2841.75	-2681.96	712.08
Fixation L	1	1	5800519743	5800638998	119255	1587.38	275.78	71	26	-1	14.86	14.86
Fixation R	1	1	5800519743	5800638998	119255	1587.38	275.78	71	26	-1	15.13	15.13
Saccade L	1	2	5800638998	5800658846	19848	1594.16	282.91	1589.48	290.31	0.15	12.65	1.00	7.75	-94.88	-173.14	126.39
Saccade R	1	2	5800638998	5800658846	19848	1594.16	282.91	1589.48	290.31	0.15	12.65	1.00	7.75	-94.88	-173.14	126.39
Fixation L	1	2	5800658846	5800758345	99499	1585.15	294.71	9	13	-1	14.66	14.66
Fixation R	1	2	5800658846	5800758345	99499	1585.15	294.71	9	13	-1	14.30	14.30
Blink L	1	1	5800758345	5801355074	596729
Blink R	1	1	5800758345	5801355074	596729
Saccade L	1	3	5801355074	5801434563	79489	885.21	464.68	838.86	496.63	12.83	976.61	1.00	161.46	24121.15	-886.51	4330.62
Saccade R	1	3	5801355074	5801434563	79489	885.21	464.68	838.86	496.63	12.83	976.61	1.00	161.46	24121.15	-886.51	4330.62
Blink L	1	2	5801434563	5801713054	278491
Blink R	1	2	5801434563	5801713054	278491
Fixation L	1	3	5801713054	5802349518	636464	886.53	459.22	44	26	-1	14.65	14.65
Fixation R	1	3	5801713054	5802349518	636464	886.53	459.22	44	26	-1	14.29	14.29
