[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S22H22551-finaleart-1.idf
Date:	27.03.2019 15:57:19
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S22H22551
Description:	Mathieu Chambre

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	4941309743	# Message: void.jpg
Fixation L	1	1	4941313581	4942268275	954694	1225.01	341.41	34	63	-1	16.56	16.56
Fixation R	1	1	4941313581	4942268275	954694	1225.01	341.41	34	63	-1	16.43	16.43
Saccade L	1	1	4942268275	4942288148	19873	1204.26	379.72	1202.46	382.35	0.04	3.22	1.00	2.16	25.73	-24.03	21.74
Saccade R	1	1	4942268275	4942288148	19873	1204.26	379.72	1202.46	382.35	0.04	3.22	1.00	2.16	25.73	-24.03	21.74
Fixation L	1	2	4942288148	4942845116	556968	1195.39	395.01	14	32	-1	18.53	18.53
Fixation R	1	2	4942288148	4942845116	556968	1195.39	395.01	14	32	-1	18.31	18.31
Saccade L	1	2	4942845116	4942864997	19881	1187.73	412.89	1124.67	413.82	0.40	63.79	1.00	20.20	1518.41	-1237.00	1022.21
Saccade R	1	2	4942845116	4942864997	19881	1187.73	412.89	1124.67	413.82	0.40	63.79	1.00	20.20	1518.41	-1237.00	1022.21
Fixation L	1	3	4942864997	4943302599	437602	1114.25	421.21	35	14	-1	19.60	19.60
Fixation R	1	3	4942864997	4943302599	437602	1114.25	421.21	35	14	-1	19.57	19.57
