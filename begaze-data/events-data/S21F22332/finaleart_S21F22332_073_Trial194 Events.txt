[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S21F22332-finaleart-1.idf
Date:	27.03.2019 15:55:46
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S21F22332
Description:	Cyrielle Mailart

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	2802668517	# Message: void.jpg
Fixation L	1	1	2802673984	2802773355	99371	1147.57	606.53	66	21	-1	11.92	11.92
Fixation R	1	1	2802673984	2802773355	99371	1147.57	606.53	66	21	-1	11.19	11.19
Saccade L	1	1	2802773355	2802813216	39861	1118.60	612.52	721.67	470.96	3.61	223.29	1.00	90.61	5437.22	-5204.19	5192.98
Saccade R	1	1	2802773355	2802813216	39861	1118.60	612.52	721.67	470.96	3.61	223.29	1.00	90.61	5437.22	-5204.19	5192.98
Fixation L	1	2	2802813216	2803111573	298357	714.84	482.70	11	23	-1	11.90	11.90
Fixation R	1	2	2802813216	2803111573	298357	714.84	482.70	11	23	-1	11.58	11.58
Blink L	1	1	2803111573	2804049395	937822
Blink R	1	1	2803111573	2804049395	937822
Fixation L	1	3	2804049395	2804447126	397731	922.42	390.74	11	72	-1	11.24	11.24
Fixation R	1	3	2804049395	2804486996	437601	922.51	392.30	15	72	-1	10.33	10.33
Blink L	1	2	2804447126	2804586367	139241
Saccade R	1	2	2804486996	2804546616	59620	930.91	401.86	1000.64	879.90	5.69	168.11	1.00	95.46	3368.71	-3869.94	2746.89
Fixation R	1	4	2804546616	2804665987	119371	1022.65	875.35	30	10	-1	9.95	9.95
Saccade L	1	2	2804586367	2804665987	79620	1021.04	870.46	1030.09	869.56	0.92	30.75	1.00	11.51	35.04	-628.99	175.81
