[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S21F22332-finaleart-1.idf
Date:	27.03.2019 15:54:48
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S21F22332
Description:	Cyrielle Mailart

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	2265318189	# Message: void.jpg
Fixation L	1	1	2265330466	2265807815	477349	1362.43	807.48	70	28	-1	12.72	12.72
Fixation R	1	1	2265330466	2265807815	477349	1362.43	807.48	70	28	-1	11.84	11.84
Saccade L	1	1	2265807815	2265827687	19872	1383.79	821.48	1382.45	823.20	0.23	39.43	1.00	11.43	39150.97	31.97	13363.26
Saccade R	1	1	2265807815	2265827687	19872	1383.79	821.48	1382.45	823.20	0.23	39.43	1.00	11.43	39150.97	31.97	13363.26
Blink L	1	1	2265827687	2266106173	278486
Blink R	1	1	2265827687	2266106173	278486
Fixation L	1	2	2266106173	2266543648	437475	1121.01	526.46	26	66	-1	14.17	14.17
Fixation R	1	2	2266106173	2266543648	437475	1121.01	526.46	26	66	-1	14.02	14.02
Blink L	1	2	2266543648	2266722765	179117
Blink R	1	2	2266543648	2266722765	179117
Fixation L	1	3	2266722765	2267319361	596596	1032.15	504.67	36	47	-1	13.90	13.90
Fixation R	1	3	2266722765	2267319361	596596	1032.15	504.67	36	47	-1	13.06	13.06
