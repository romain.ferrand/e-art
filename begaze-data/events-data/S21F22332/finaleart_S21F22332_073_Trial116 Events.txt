[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S21F22332-finaleart-1.idf
Date:	27.03.2019 15:55:11
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S21F22332
Description:	Cyrielle Mailart

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	2498462377	# Message: void.jpg
Fixation L	1	1	2498469571	2498847556	377985	1042.17	137.26	50	45	-1	13.58	13.58
Fixation R	1	1	2498469571	2498847556	377985	1042.17	137.26	50	45	-1	13.83	13.83
Saccade L	1	1	2498847556	2498947033	99477	1073.08	152.33	1079.23	161.56	2.90	185.14	1.00	29.20	28265.43	-254.37	4762.08
Saccade R	1	1	2498847556	2498947033	99477	1073.08	152.33	1079.23	161.56	2.90	185.14	1.00	29.20	28265.43	-254.37	4762.08
Blink L	1	1	2499026540	2499285149	258609
Blink R	1	1	2499026540	2499285149	258609
Fixation L	1	2	2499285149	2500200118	914969	1051.68	248.00	15	77	-1	14.32	14.32
Fixation R	1	2	2499285149	2500200118	914969	1051.68	248.00	15	77	-1	13.50	13.50
Saccade L	1	2	2500200118	2500219977	19859	1046.77	248.52	1115.06	243.80	0.48	69.24	1.00	24.07	1665.18	-1359.93	1177.33
Saccade R	1	2	2500200118	2500219977	19859	1046.77	248.52	1115.06	243.80	0.48	69.24	1.00	24.07	1665.18	-1359.93	1177.33
Fixation L	1	3	2500219977	2500458587	238610	1114.64	232.43	30	32	-1	14.69	14.69
Fixation R	1	3	2500219977	2500458587	238610	1114.64	232.43	30	32	-1	13.64	13.64
