[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S21F22332-finaleart-1.idf
Date:	27.03.2019 15:56:18
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S21F22332
Description:	Cyrielle Mailart

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	3132044126	# Message: void.jpg
Fixation L	1	1	3132053079	3132530545	477466	1049.34	362.58	34	32	-1	14.33	14.33
Fixation R	1	1	3132053079	3132530545	477466	1049.34	362.58	34	32	-1	14.25	14.25
Blink L	1	1	3132530545	3132769161	238616
Blink R	1	1	3132530545	3132769161	238616
Fixation L	1	2	3132769161	3133007896	238735	933.41	373.55	13	28	-1	11.78	11.78
Fixation R	1	2	3132769161	3133007896	238735	933.41	373.55	13	28	-1	11.48	11.48
Blink L	1	2	3133007896	3133266383	258487
Blink R	1	2	3133007896	3133266383	258487
Fixation L	1	3	3133266383	3133763612	497229	880.26	380.64	59	37	-1	13.26	13.26
Fixation R	1	3	3133266383	3133763612	497229	880.26	380.64	59	37	-1	12.80	12.80
Saccade L	1	1	3133763612	3133783613	20001	924.92	402.32	901.21	407.70	0.43	41.68	0.00	21.66	584.22	-563.91	540.87
Saccade R	1	1	3133763612	3133783613	20001	924.92	402.32	901.21	407.70	0.43	41.68	0.00	21.66	584.22	-563.91	540.87
Fixation L	1	4	3133783613	3134042089	258476	935.75	425.34	43	30	-1	13.99	13.99
Fixation R	1	4	3133783613	3134042089	258476	935.75	425.34	43	30	-1	13.75	13.75
