[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S21F22332-finaleart-1.idf
Date:	27.03.2019 15:55:50
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S21F22332
Description:	Cyrielle Mailart

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	2833208133	# Message: void.jpg
Fixation L	1	1	2833227887	2833367124	139237	1569.18	575.98	11	27	-1	15.81	15.81
Fixation R	1	1	2833227887	2833367124	139237	1569.18	575.98	11	27	-1	14.12	14.12
Saccade L	1	1	2833367124	2833386993	19869	1571.40	585.50	1694.04	580.83	0.99	124.13	1.00	49.75	3044.58	-2060.48	2193.71
Saccade R	1	1	2833367124	2833386993	19869	1571.40	585.50	1694.04	580.83	0.99	124.13	1.00	49.75	3044.58	-2060.48	2193.71
Fixation L	1	2	2833386993	2833625733	238740	1659.40	589.12	64	14	-1	15.17	15.17
Fixation R	1	2	2833386993	2833625733	238740	1659.40	589.12	64	14	-1	13.47	13.47
Blink L	1	1	2833625733	2833943959	318226
Blink R	1	1	2833625733	2833943959	318226
Fixation L	1	3	2833943959	2834381569	437610	1235.71	460.24	18	34	-1	14.21	14.21
Fixation R	1	3	2833943959	2834381569	437610	1235.71	460.24	18	34	-1	12.31	12.31
Blink L	1	2	2834381569	2834580426	198857
Blink R	1	2	2834381569	2834580426	198857
Fixation L	1	4	2834580426	2834739553	159127	1263.48	369.65	4	30	-1	14.07	14.07
Fixation R	1	4	2834580426	2834739553	159127	1263.48	369.65	4	30	-1	12.51	12.51
Saccade L	1	2	2834739553	2834779300	39747	1262.19	378.04	1144.35	458.21	2.72	192.92	0.50	68.35	4575.00	-4279.65	3432.12
Saccade R	1	2	2834739553	2834779300	39747	1262.19	378.04	1144.35	458.21	2.72	192.92	0.50	68.35	4575.00	-4279.65	3432.12
Fixation L	1	5	2834779300	2835197029	417729	1132.97	453.92	13	25	-1	14.27	14.27
Fixation R	1	5	2834779300	2835197029	417729	1132.97	453.92	13	25	-1	12.53	12.53
