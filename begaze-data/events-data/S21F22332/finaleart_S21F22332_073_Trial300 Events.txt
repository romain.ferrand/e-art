[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S21F22332-finaleart-1.idf
Date:	27.03.2019 15:56:33
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S21F22332
Description:	Cyrielle Mailart

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	3236140489	# Message: void.jpg
Fixation L	1	1	3236157150	3237827825	1670675	961.39	551.17	43	53	-1	13.83	13.83
Fixation R	1	1	3236157150	3237827825	1670675	961.39	551.17	43	53	-1	12.22	12.22
Saccade L	1	1	3237827825	3237847696	19871	980.21	541.07	1031.38	597.93	0.50	77.38	1.00	25.40	1907.67	-1686.43	1345.08
Saccade R	1	1	3237827825	3237847696	19871	980.21	541.07	1031.38	597.93	0.50	77.38	1.00	25.40	1907.67	-1686.43	1345.08
Fixation L	1	2	3237847696	3238086430	238734	1044.61	590.34	23	23	-1	14.47	14.47
Fixation R	1	2	3237847696	3238086430	238734	1044.61	590.34	23	23	-1	12.63	12.63
