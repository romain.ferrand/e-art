[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S21F22332-finaleart-1.idf
Date:	27.03.2019 15:54:25
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S21F22332
Description:	Cyrielle Mailart

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	1693730701	# Message: void.jpg
Fixation L	1	1	1693735047	1693914034	178987	1690.14	1020.76	25	8	-1	12.25	12.25
Fixation R	1	1	1693735047	1693914034	178987	1690.14	1020.76	25	8	-1	10.70	10.70
Saccade L	1	1	1693914034	1693953906	39872	1688.66	1019.24	1403.15	500.98	5.05	335.98	0.50	126.67	8255.32	-7947.52	7221.73
Saccade R	1	1	1693914034	1693953906	39872	1688.66	1019.24	1403.15	500.98	5.05	335.98	0.50	126.67	8255.32	-7947.52	7221.73
Fixation L	1	2	1693953906	1694232258	278352	1390.39	444.28	17	75	-1	12.90	12.90
Fixation R	1	2	1693953906	1694232258	278352	1390.39	444.28	17	75	-1	11.17	11.17
Blink L	1	1	1694232258	1694550622	318364
Blink R	1	1	1694232258	1694550622	318364
Fixation L	1	3	1694550622	1695067717	517095	1052.31	378.85	13	54	-1	15.94	15.94
Fixation R	1	3	1694550622	1695067717	517095	1052.31	378.85	13	54	-1	14.19	14.19
Blink L	1	2	1695067717	1695346202	278485
Blink R	1	2	1695067717	1695346202	278485
Fixation L	1	4	1695346202	1695724065	377863	926.31	473.71	13	23	-1	16.80	16.80
Fixation R	1	4	1695346202	1695724065	377863	926.31	473.71	13	23	-1	14.75	14.75
