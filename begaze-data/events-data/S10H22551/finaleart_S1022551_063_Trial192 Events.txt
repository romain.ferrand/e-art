[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S1022551-finaleart-1.idf
Date:	27.03.2019 15:38:07
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S1022551
Description:	Alexis Nebout

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	8489729558	# Message: void.jpg
Saccade L	1	1	8489749384	8489809016	59632	1166.18	535.36	633.53	530.42	6.50	314.88	0.67	108.92	7871.94	-7721.37	3721.05
Saccade R	1	1	8489749384	8489809016	59632	1166.18	535.36	633.53	530.42	6.50	314.88	0.67	108.92	7871.94	-7721.37	3721.05
Fixation L	1	1	8489809016	8490087023	278007	633.58	543.84	9	21	-1	9.92	9.92
Fixation R	1	1	8489809016	8490087023	278007	633.58	543.84	9	21	-1	9.58	9.58
Saccade L	1	2	8490087023	8490107356	20333	637.42	551.38	641.53	360.19	1.07	193.39	1.00	52.63	4740.81	-4731.82	3219.44
Saccade R	1	2	8490087023	8490107356	20333	637.42	551.38	641.53	360.19	1.07	193.39	1.00	52.63	4740.81	-4731.82	3219.44
Fixation L	1	2	8490107356	8490644468	537112	665.93	375.67	39	21	-1	9.53	9.53
Fixation R	1	2	8490107356	8490644468	537112	665.93	375.67	39	21	-1	9.45	9.45
Blink L	1	1	8490644468	8490902942	258474
Blink R	1	1	8490644468	8490902942	258474
Fixation L	1	3	8490902942	8491718531	815589	897.79	455.24	32	56	-1	11.53	11.53
Fixation R	1	3	8490902942	8491718531	815589	897.79	455.24	32	56	-1	10.63	10.63
