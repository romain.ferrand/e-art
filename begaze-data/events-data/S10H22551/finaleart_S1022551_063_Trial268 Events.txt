[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S1022551-finaleart-1.idf
Date:	27.03.2019 15:38:23
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S1022551
Description:	Alexis Nebout

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	8911681333	# Message: void.jpg
Fixation L	1	1	8911700575	8911959180	258605	917.27	611.50	15	20	-1	10.51	10.51
Fixation R	1	1	8911700575	8911959180	258605	917.27	611.50	15	20	-1	9.46	9.46
Saccade L	1	1	8911959180	8912028227	69047	919.75	604.08	1098.97	602.46	4.08	181.25	1.00	59.07	4398.89	-4332.31	3280.40
Saccade R	1	1	8911959180	8912028227	69047	919.75	604.08	1098.97	602.46	4.08	181.25	1.00	59.07	4398.89	-4332.31	3280.40
Fixation L	1	2	8912028227	8912536029	507802	1160.09	613.67	69	22	-1	10.59	10.59
Fixation R	1	2	8912028227	8912536029	507802	1160.09	613.67	69	22	-1	9.26	9.26
Blink L	1	1	8912536029	8912675263	139234
Blink R	1	1	8912536029	8912675263	139234
Fixation L	1	3	8912675263	8913013378	338115	1048.66	520.20	31	40	-1	12.55	12.55
Fixation R	1	3	8912675263	8913013378	338115	1048.66	520.20	31	40	-1	10.87	10.87
Saccade L	1	2	8913013378	8913033251	19873	1039.02	518.53	917.87	527.40	0.68	122.86	1.00	34.05	3034.25	-3016.93	2057.66
Saccade R	1	2	8913013378	8913033251	19873	1039.02	518.53	917.87	527.40	0.68	122.86	1.00	34.05	3034.25	-3016.93	2057.66
Fixation L	1	4	8913033251	8913669719	636468	909.80	521.01	43	23	-1	13.45	13.45
Fixation R	1	4	8913033251	8913669719	636468	909.80	521.01	43	23	-1	11.30	11.30
