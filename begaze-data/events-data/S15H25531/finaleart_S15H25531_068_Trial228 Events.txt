[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S15H25531-finaleart-1.idf
Date:	27.03.2019 15:45:35
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S15H25531
Description:	Mikail Demirdelen

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	4508846451	# Message: void.jpg
Fixation L	1	1	4508857379	4508956754	99375	874.40	557.95	4	11	-1	13.64	13.64
Fixation R	1	1	4508857379	4508976626	119247	872.97	562.46	11	34	-1	15.14	15.14
Blink L	1	1	4508956754	4509056228	99474
Saccade R	1	1	4508976626	4509076100	99474	864.36	589.50	871.36	454.26	10.47	364.85	0.40	105.20	8527.35	-8984.44	5088.01
Saccade L	1	1	4509056228	4509076100	19872	868.75	522.21	871.36	454.26	0.55	68.79	1.00	27.56	963.12	-1565.39	845.02
Fixation L	1	2	4509076100	4509473978	397878	893.66	435.61	32	39	-1	13.83	13.83
Fixation R	1	2	4509076100	4509473978	397878	893.66	435.61	32	39	-1	14.64	14.64
Blink L	1	2	4509473978	4509692700	218722
Blink R	1	1	4509473978	4509692700	218722
Fixation L	1	3	4509692700	4510289299	596599	832.78	507.00	11	30	-1	15.17	15.17
Fixation R	1	3	4509692700	4510289299	596599	832.78	507.00	11	30	-1	15.94	15.94
Saccade L	1	2	4510289299	4510329175	39876	830.98	516.06	991.19	399.58	2.03	122.12	0.50	50.87	2946.30	-2558.48	2578.08
Saccade R	1	2	4510289299	4510329175	39876	830.98	516.06	991.19	399.58	2.03	122.12	0.50	50.87	2946.30	-2558.48	2578.08
Fixation L	1	4	4510329175	4510846269	517094	994.96	433.58	13	43	-1	14.98	14.98
Fixation R	1	4	4510329175	4510846269	517094	994.96	433.58	13	43	-1	16.09	16.09
