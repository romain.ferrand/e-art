[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S15H25531-finaleart-1.idf
Date:	27.03.2019 15:45:27
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S15H25531
Description:	Mikail Demirdelen

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	4335278929	# Message: void.jpg
Fixation L	1	1	4335288422	4336342616	1054194	933.64	555.20	31	65	-1	13.71	13.71
Fixation R	1	1	4335288422	4336342616	1054194	933.64	555.20	31	65	-1	14.55	14.55
Saccade L	1	1	4336342616	4336362495	19879	922.23	524.54	920.65	518.27	0.07	6.54	1.00	3.71	129.14	-39.14	92.64
Saccade R	1	1	4336342616	4336362495	19879	922.23	524.54	920.65	518.27	0.07	6.54	1.00	3.71	129.14	-39.14	92.64
Fixation L	1	2	4336362495	4337158067	795572	920.30	535.44	24	75	-1	13.79	13.79
Fixation R	1	2	4336362495	4337158067	795572	920.30	535.44	24	75	-1	14.69	14.69
Saccade L	1	2	4337158067	4337178075	20008	903.56	570.81	899.92	572.56	0.13	8.30	1.00	6.51	116.70	-73.52	77.35
Saccade R	1	2	4337158067	4337178075	20008	903.56	570.81	899.92	572.56	0.13	8.30	1.00	6.51	116.70	-73.52	77.35
Fixation L	1	3	4337178075	4337277460	99385	893.14	584.41	12	19	-1	14.21	14.21
Fixation R	1	3	4337178075	4337277460	99385	893.14	584.41	12	19	-1	14.82	14.82
