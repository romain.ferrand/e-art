[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S15H25531-finaleart-1.idf
Date:	27.03.2019 15:44:31
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S15H25531
Description:	Mikail Demirdelen

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	3426952449	# Message: resting-bacchante_sorolla_1887.jpg
Fixation L	1	1	3426958586	3427137560	178974	637.03	968.71	10	17	-1	15.22	15.22
Fixation R	1	1	3426958586	3427137560	178974	637.03	968.71	10	17	-1	17.67	17.67
Saccade L	1	1	3427137560	3427197196	59636	639.93	975.48	1006.68	493.04	6.78	236.28	0.67	113.77	5853.18	-5118.97	4360.90
Saccade R	1	1	3427137560	3427197196	59636	639.93	975.48	1006.68	493.04	6.78	236.28	0.67	113.77	5853.18	-5118.97	4360.90
Fixation L	1	2	3427197196	3427435939	238743	1045.68	468.18	50	37	-1	14.79	14.79
Fixation R	1	2	3427197196	3427435939	238743	1045.68	468.18	50	37	-1	17.16	17.16
Saccade L	1	2	3427435939	3427455808	19869	1048.57	469.47	848.60	614.46	1.42	249.71	1.00	71.66	6086.30	-5485.11	4026.87
Saccade R	1	2	3427435939	3427455808	19869	1048.57	469.47	848.60	614.46	1.42	249.71	1.00	71.66	6086.30	-5485.11	4026.87
Fixation L	1	3	3427455808	3427813790	357982	862.32	593.99	25	33	-1	14.22	14.22
Fixation R	1	3	3427455808	3427813790	357982	862.32	593.99	25	33	-1	16.40	16.40
Saccade L	1	3	3427813790	3427833793	20003	857.23	590.52	653.03	601.52	1.21	206.78	1.00	60.64	5156.48	-4900.56	3608.28
Saccade R	1	3	3427813790	3427833793	20003	857.23	590.52	653.03	601.52	1.21	206.78	1.00	60.64	5156.48	-4900.56	3608.28
Fixation L	1	4	3427833793	3427972901	139108	668.46	635.14	24	53	-1	13.59	13.59
Fixation R	1	4	3427833793	3427972901	139108	668.46	635.14	24	53	-1	15.80	15.80
Saccade L	1	4	3427972901	3427992911	20010	668.86	650.45	535.97	612.73	0.83	139.71	1.00	41.26	3329.91	-3220.14	2258.17
Saccade R	1	4	3427972901	3427992911	20010	668.86	650.45	535.97	612.73	0.83	139.71	1.00	41.26	3329.91	-3220.14	2258.17
Fixation L	1	5	3427992911	3428112148	119237	532.80	598.90	11	50	-1	13.18	13.18
Fixation R	1	5	3427992911	3428112148	119237	532.80	598.90	11	50	-1	15.08	15.08
Saccade L	1	5	3428112148	3428132019	19871	527.77	576.01	483.66	570.16	0.56	57.38	1.00	28.37	1380.83	-1015.27	1099.38
Saccade R	1	5	3428112148	3428132019	19871	527.77	576.01	483.66	570.16	0.56	57.38	1.00	28.37	1380.83	-1015.27	1099.38
Fixation L	1	6	3428132019	3428331005	198986	430.79	594.42	62	33	-1	12.74	12.74
Fixation R	1	6	3428132019	3428331005	198986	430.79	594.42	62	33	-1	14.84	14.84
Saccade L	1	6	3428331005	3428390637	59632	421.88	603.67	296.11	429.84	3.29	133.88	0.33	55.22	3296.80	-2617.97	2409.24
Saccade R	1	6	3428331005	3428390637	59632	421.88	603.67	296.11	429.84	3.29	133.88	0.33	55.22	3296.80	-2617.97	2409.24
Fixation L	1	7	3428390637	3428589501	198864	325.87	445.83	45	47	-1	12.56	12.56
Fixation R	1	7	3428390637	3428589501	198864	325.87	445.83	45	47	-1	14.79	14.79
Saccade L	1	7	3428589501	3428609501	20000	328.03	429.34	330.33	411.51	0.17	18.18	1.00	8.31	323.39	-286.82	236.03
Saccade R	1	7	3428589501	3428609501	20000	328.03	429.34	330.33	411.51	0.17	18.18	1.00	8.31	323.39	-286.82	236.03
Fixation L	1	8	3428609501	3428728746	119245	328.93	413.34	9	24	-1	12.72	12.72
Fixation R	1	8	3428609501	3428728746	119245	328.93	413.34	9	24	-1	14.63	14.63
Saccade L	1	8	3428728746	3428748621	19875	333.97	428.34	451.62	559.53	1.08	178.21	1.00	54.50	4223.75	-3918.57	2746.74
Saccade R	1	8	3428728746	3428748621	19875	333.97	428.34	451.62	559.53	1.08	178.21	1.00	54.50	4223.75	-3918.57	2746.74
Fixation L	1	9	3428748621	3429007234	258613	476.29	585.60	62	36	-1	12.97	12.97
Fixation R	1	9	3428748621	3429007234	258613	476.29	585.60	62	36	-1	15.22	15.22
Saccade L	1	9	3429007234	3429027103	19869	512.60	584.32	521.83	583.27	0.16	13.13	1.00	7.89	3.84	-106.89	68.05
Saccade R	1	9	3429007234	3429027103	19869	512.60	584.32	521.83	583.27	0.16	13.13	1.00	7.89	3.84	-106.89	68.05
Fixation L	1	10	3429027103	3429703455	676352	548.56	577.97	50	40	-1	13.77	13.77
Fixation R	1	10	3429027103	3429703455	676352	548.56	577.97	50	40	-1	16.14	16.14
Saccade L	1	10	3429703455	3429723318	19863	532.02	554.69	478.06	552.82	0.43	54.61	1.00	21.66	1250.41	-985.34	890.36
Saccade R	1	10	3429703455	3429723318	19863	532.02	554.69	478.06	552.82	0.43	54.61	1.00	21.66	1250.41	-985.34	890.36
Fixation L	1	11	3429723318	3430200676	477358	479.43	546.54	77	19	-1	13.88	13.88
Fixation R	1	11	3429723318	3430200676	477358	479.43	546.54	77	19	-1	16.15	16.15
Saccade L	1	11	3430200676	3430260297	59621	527.63	535.77	337.39	422.74	3.49	201.08	0.67	58.47	5013.11	-4889.47	2715.94
Saccade R	1	11	3430200676	3430260297	59621	527.63	535.77	337.39	422.74	3.49	201.08	0.67	58.47	5013.11	-4889.47	2715.94
Fixation L	1	12	3430260297	3430518912	258615	347.52	465.48	24	69	-1	14.04	14.04
Fixation R	1	12	3430260297	3430518912	258615	347.52	465.48	24	69	-1	16.77	16.77
Saccade L	1	12	3430518912	3430598401	79489	361.38	486.85	1382.41	519.94	12.34	328.38	0.50	155.24	8143.87	-7082.42	5134.98
Saccade R	1	12	3430518912	3430598401	79489	361.38	486.85	1382.41	519.94	12.34	328.38	0.50	155.24	8143.87	-7082.42	5134.98
Fixation L	1	13	3430598401	3430777388	178987	1377.59	551.50	41	49	-1	14.56	14.56
Fixation R	1	13	3430598401	3430777388	178987	1377.59	551.50	41	49	-1	16.46	16.46
Saccade L	1	13	3430777388	3430797400	20012	1405.48	553.39	1555.61	467.70	1.07	174.82	1.00	53.45	4338.15	-3704.01	2843.71
Saccade R	1	13	3430777388	3430797400	20012	1405.48	553.39	1555.61	467.70	1.07	174.82	1.00	53.45	4338.15	-3704.01	2843.71
Fixation L	1	14	3430797400	3430936513	139113	1541.99	489.47	19	39	-1	14.17	14.17
Fixation R	1	14	3430797400	3430936513	139113	1541.99	489.47	19	39	-1	16.18	16.18
