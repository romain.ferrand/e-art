[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S15H25531-finaleart-1.idf
Date:	27.03.2019 15:44:37
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S15H25531
Description:	Mikail Demirdelen

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	3479879683	# Message: void.jpg
Fixation L	1	1	3479885927	3480025171	139244	607.43	913.13	67	4	-1	13.17	13.17
Fixation R	1	1	3479885927	3480025171	139244	607.43	913.13	67	4	-1	14.89	14.89
Saccade L	1	1	3480025171	3480064914	39743	660.70	912.14	731.97	842.97	1.41	58.63	0.00	35.50	1184.90	-1350.84	865.70
Saccade R	1	1	3480025171	3480064914	39743	660.70	912.14	731.97	842.97	1.41	58.63	0.00	35.50	1184.90	-1350.84	865.70
Fixation L	1	2	3480064914	3480979864	914950	732.44	822.82	30	30	-1	13.32	13.32
Fixation R	1	2	3480064914	3480979864	914950	732.44	822.82	30	30	-1	14.89	14.89
Saccade L	1	2	3480979864	3481039489	59625	726.93	814.85	858.26	519.79	3.66	218.02	0.33	61.46	5398.13	-4385.30	2725.91
Saccade R	1	2	3480979864	3481039489	59625	726.93	814.85	858.26	519.79	3.66	218.02	0.33	61.46	5398.13	-4385.30	2725.91
Fixation L	1	3	3481039489	3481874954	835465	886.15	498.52	42	41	-1	14.40	14.40
Fixation R	1	3	3481039489	3481874954	835465	886.15	498.52	42	41	-1	15.70	15.70
