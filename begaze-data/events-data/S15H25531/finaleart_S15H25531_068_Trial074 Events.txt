[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S15H25531-finaleart-1.idf
Date:	27.03.2019 15:44:44
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S15H25531
Description:	Mikail Demirdelen

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	3819999457	# Message: void.jpg
Fixation L	1	1	3820002925	3820480262	477337	824.88	782.95	71	27	-1	13.56	13.56
Fixation R	1	1	3820002925	3820480262	477337	824.88	782.95	71	27	-1	14.77	14.77
Saccade L	1	1	3820480262	3820500142	19880	859.30	767.71	858.26	762.36	0.15	15.28	0.00	7.45	49.71	-245.08	111.02
Saccade R	1	1	3820480262	3820500142	19880	859.30	767.71	858.26	762.36	0.15	15.28	0.00	7.45	49.71	-245.08	111.02
Fixation L	1	2	3820500142	3821991954	1491812	863.66	779.34	29	50	-1	14.40	14.40
Fixation R	1	2	3820500142	3821991954	1491812	863.66	779.34	29	50	-1	15.89	15.89
