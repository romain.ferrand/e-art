[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S5121332-finaleart-1.idf
Date:	27.03.2019 15:35:33
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S5121332
Description:	Dylan Marinho

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	10415319846	# Message: void.jpg
Fixation L	1	1	10415338512	10415537483	198971	867.82	775.61	85	13	-1	15.47	15.47
Fixation R	1	1	10415338512	10415537483	198971	867.82	775.61	85	13	-1	15.23	15.23
Saccade L	1	1	10415537483	10415557367	19884	900.53	769.09	901.11	767.20	0.04	3.24	0.00	2.24	15.23	-52.67	27.47
Saccade R	1	1	10415537483	10415557367	19884	900.53	769.09	901.11	767.20	0.04	3.24	0.00	2.24	15.23	-52.67	27.47
Fixation L	1	2	10415557367	10415676725	119358	903.34	765.12	4	3	-1	15.72	15.72
Fixation R	1	2	10415557367	10415676725	119358	903.34	765.12	4	3	-1	15.27	15.27
Blink L	1	1	10415676725	10415835837	159112
Blink R	1	1	10415676725	10415835837	159112
Fixation L	1	3	10415835837	10415994977	159140	976.84	578.05	5	42	-1	14.54	14.54
Fixation R	1	3	10415835837	10415994977	159140	976.84	578.05	5	42	-1	14.86	14.86
Blink L	1	2	10415994977	10416173965	178988
Blink R	1	2	10415994977	10416173965	178988
Fixation L	1	4	10416173965	10417307635	1133670	979.28	567.55	14	53	-1	13.73	13.73
Fixation R	1	4	10416173965	10417307635	1133670	979.28	567.55	14	53	-1	13.40	13.40
