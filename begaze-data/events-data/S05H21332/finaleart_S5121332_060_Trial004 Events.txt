[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S5121332-finaleart-1.idf
Date:	27.03.2019 15:34:53
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S5121332
Description:	Dylan Marinho

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	8903683906	# Message: void.jpg
Fixation L	1	1	8903700710	8904277418	576708	924.61	613.29	51	43	-1	18.56	18.56
Fixation R	1	1	8903700710	8904277418	576708	924.61	613.29	51	43	-1	19.00	19.00
Saccade L	1	1	8904277418	8904297424	20006	932.29	601.58	932.30	591.31	0.13	10.40	1.00	6.53	176.62	-225.34	178.65
Saccade R	1	1	8904277418	8904297424	20006	932.29	601.58	932.30	591.31	0.13	10.40	1.00	6.53	176.62	-225.34	178.65
Fixation L	1	2	8904297424	8904456559	159135	927.04	589.96	7	17	-1	16.29	16.29
Fixation R	1	2	8904297424	8904456559	159135	927.04	589.96	7	17	-1	16.62	16.62
Blink L	1	1	8904456559	8904675286	218727
Blink R	1	1	8904456559	8904675286	218727
Fixation L	1	3	8904675286	8905689738	1014452	936.85	611.27	10	41	-1	15.36	15.36
Fixation R	1	3	8904675286	8905689738	1014452	936.85	611.27	10	41	-1	15.97	15.97
