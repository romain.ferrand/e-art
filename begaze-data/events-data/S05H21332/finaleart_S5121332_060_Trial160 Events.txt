[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S5121332-finaleart-1.idf
Date:	27.03.2019 15:35:15
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S5121332
Description:	Dylan Marinho

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	9646639706	# Message: void.jpg
Blink L	1	1	9646649004	9646947352	298348
Blink R	1	1	9646649004	9646947352	298348
Fixation L	1	1	9646947352	9647245706	298354	837.75	454.66	28	43	-1	14.78	14.78
Fixation R	1	1	9646947352	9647245706	298354	837.75	454.66	28	43	-1	14.39	14.39
Blink L	1	2	9647245706	9647663437	417731
Blink R	1	2	9647245706	9647663437	417731
Fixation L	1	2	9647663437	9647941919	278482	914.05	421.99	21	74	-1	14.37	14.37
Fixation R	1	2	9647663437	9647941919	278482	914.05	421.99	21	74	-1	14.28	14.28
Saccade L	1	1	9647941919	9647961792	19873	903.97	457.68	902.84	465.77	0.14	9.57	1.00	7.02	127.10	-140.32	109.94
Saccade R	1	1	9647941919	9647961792	19873	903.97	457.68	902.84	465.77	0.14	9.57	1.00	7.02	127.10	-140.32	109.94
Fixation L	1	3	9647961792	9648200404	238612	894.55	478.80	32	34	-1	14.85	14.85
Fixation R	1	3	9647961792	9648200404	238612	894.55	478.80	32	34	-1	14.67	14.67
Saccade L	1	2	9648200404	9648220277	19873	872.86	457.24	857.84	438.16	0.31	34.65	1.00	15.48	-33.46	-251.85	174.96
Saccade R	1	2	9648200404	9648220277	19873	872.86	457.24	857.84	438.16	0.31	34.65	1.00	15.48	-33.46	-251.85	174.96
Fixation L	1	4	9648220277	9648657876	437599	875.22	435.61	46	47	-1	14.07	14.07
Fixation R	1	4	9648220277	9648657876	437599	875.22	435.61	46	47	-1	13.96	13.96
