[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S5121332-finaleart-1.idf
Date:	27.03.2019 15:35:32
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S5121332
Description:	Dylan Marinho

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	10390663409	# Message: void.jpg
Fixation L	1	1	10390675241	10391112831	437590	707.05	615.53	62	21	-1	13.41	13.41
Fixation R	1	1	10390675241	10391112831	437590	707.05	615.53	62	21	-1	13.02	13.02
Blink L	1	1	10391112831	10391291813	178982
Blink R	1	1	10391112831	10391291813	178982
Saccade L	1	1	10391291813	10391371331	79518	825.52	555.09	843.51	566.34	2.07	111.69	1.00	25.98	26700.70	-497.10	5015.28
Saccade R	1	1	10391291813	10391371331	79518	825.52	555.09	843.51	566.34	2.07	111.69	1.00	25.98	26700.70	-497.10	5015.28
Blink L	1	2	10391371331	10391530440	159109
Blink R	1	2	10391371331	10391530440	159109
Fixation L	1	2	10391530440	10392644234	1113794	959.81	545.30	28	69	-1	15.24	15.24
Fixation R	1	2	10391530440	10392644234	1113794	959.81	545.30	28	69	-1	14.65	14.65
