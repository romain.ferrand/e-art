[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S5121332-finaleart-1.idf
Date:	27.03.2019 15:35:04
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S5121332
Description:	Dylan Marinho

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	9304912469	# Message: void.jpg
Saccade L	1	1	9304930126	9304949999	19873	1022.11	700.29	1023.91	698.43	0.67	98.44	1.00	33.69	32336.61	0.00	11599.21
Saccade R	1	1	9304930126	9304949999	19873	1022.11	700.29	1023.91	698.43	0.67	98.44	1.00	33.69	32336.61	0.00	11599.21
Blink L	1	1	9304949999	9305168728	218729
Blink R	1	1	9304949999	9305168728	218729
Fixation L	1	1	9305168728	9305685946	517218	1031.99	472.36	16	47	-1	16.45	16.45
Fixation R	1	1	9305168728	9305685946	517218	1031.99	472.36	16	47	-1	16.86	16.86
Blink L	1	2	9305685946	9305884807	198861
Blink R	1	2	9305685946	9305884807	198861
Fixation L	1	2	9305884807	9306919041	1034234	975.40	474.22	12	36	-1	16.63	16.63
Fixation R	1	2	9305884807	9306919041	1034234	975.40	474.22	12	36	-1	16.91	16.91
