[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S5121332-finaleart-1.idf
Date:	27.03.2019 15:35:12
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S5121332
Description:	Dylan Marinho

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	9572601678	# Message: void.jpg
Fixation L	1	1	9572611469	9573088806	477337	911.58	486.03	11	14	-1	15.81	15.81
Fixation R	1	1	9572611469	9573088806	477337	911.58	486.03	11	14	-1	15.60	15.60
Blink L	1	1	9573088806	9573267799	178993
Blink R	1	1	9573088806	9573267799	178993
Saccade L	1	1	9573267799	9573327416	59617	962.34	444.07	972.39	454.29	2.20	153.49	1.00	36.98	28283.73	-867.12	6604.58
Saccade R	1	1	9573267799	9573327416	59617	962.34	444.07	972.39	454.29	2.20	153.49	1.00	36.98	28283.73	-867.12	6604.58
Blink L	1	2	9573327416	9573506400	178984
Blink R	1	2	9573327416	9573506400	178984
Fixation L	1	2	9573506400	9574620222	1113822	1009.86	491.48	21	55	-1	14.64	14.64
Fixation R	1	2	9573506400	9574620222	1113822	1009.86	491.48	21	55	-1	14.65	14.65
