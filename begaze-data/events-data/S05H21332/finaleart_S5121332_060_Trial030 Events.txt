[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S5121332-finaleart-1.idf
Date:	27.03.2019 15:34:57
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S5121332
Description:	Dylan Marinho

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	9033929733	# Message: void.jpg
Fixation L	1	1	9033937101	9034334944	397843	674.81	786.18	82	14	-1	15.95	15.95
Fixation R	1	1	9033937101	9034334944	397843	674.81	786.18	82	14	-1	16.42	16.42
Saccade L	1	1	9034334944	9034354822	19878	729.10	787.72	723.11	796.66	5.42	1075.59	1.00	272.74	26830.76	-272.22	9107.46
Saccade R	1	1	9034334944	9034354822	19878	729.10	787.72	723.11	796.66	5.42	1075.59	1.00	272.74	26830.76	-272.22	9107.46
Blink L	1	1	9034354822	9034573576	218754
Blink R	1	1	9034354822	9034573576	218754
Fixation L	1	2	9034573576	9035409017	835441	910.28	583.05	10	54	-1	16.95	16.95
Fixation R	1	2	9034573576	9035409017	835441	910.28	583.05	10	54	-1	17.12	17.12
Blink L	1	2	9035409017	9035607878	198861
Blink R	1	2	9035409017	9035607878	198861
Fixation L	1	3	9035607878	9035926099	318221	873.56	559.02	6	15	-1	16.09	16.09
Fixation R	1	3	9035607878	9035926099	318221	873.56	559.02	6	15	-1	16.49	16.49
