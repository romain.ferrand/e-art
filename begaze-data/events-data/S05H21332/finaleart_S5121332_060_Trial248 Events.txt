[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S5121332-finaleart-1.idf
Date:	27.03.2019 15:35:28
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S5121332
Description:	Dylan Marinho

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	10171294971	# Message: void.jpg
Saccade L	1	1	10171301850	10171361593	59743	1016.20	286.72	1024.04	130.70	3.23	184.77	0.67	54.02	4619.24	-3204.86	2409.47
Saccade R	1	1	10171301850	10171361593	59743	1016.20	286.72	1024.04	130.70	3.23	184.77	0.67	54.02	4619.24	-3204.86	2409.47
Blink L	1	1	10171361593	10171580329	218736
Blink R	1	1	10171361593	10171580329	218736
Fixation L	1	1	10171580329	10171759312	178983	1231.19	659.21	41	22	-1	12.09	12.09
Fixation R	1	1	10171580329	10171759312	178983	1231.19	659.21	41	22	-1	12.63	12.63
Blink L	1	2	10171759312	10172974622	1215310
Blink R	1	2	10171759312	10172974622	1215310
Saccade L	1	2	10172974622	10173054111	79489	973.46	378.07	967.84	541.03	13.91	1043.68	1.00	175.05	2976.60	-26020.94	5519.89
Saccade R	1	2	10172974622	10173054111	79489	973.46	378.07	967.84	541.03	13.91	1043.68	1.00	175.05	2976.60	-26020.94	5519.89
Fixation L	1	2	10173054111	10173292856	238745	955.50	529.06	25	20	-1	13.59	13.59
Fixation R	1	2	10173054111	10173292856	238745	955.50	529.06	25	20	-1	13.05	13.05
