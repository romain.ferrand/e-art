[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S3121421-finaleart-1.idf
Date:	27.03.2019 15:34:01
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S3121421
Description:	Jean Jouve

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	4650744255	# Message: void.jpg
Saccade L	1	1	4650750579	4650849924	99345	1113.98	781.61	1052.82	673.96	2.33	88.53	1.00	23.48	2211.62	-2035.45	767.31
Saccade R	1	1	4650750579	4650849924	99345	1113.98	781.61	1052.82	673.96	2.33	88.53	1.00	23.48	2211.62	-2035.45	767.31
Fixation L	1	1	4650849924	4651148299	298375	1067.05	660.12	31	45	-1	10.88	10.88
Fixation R	1	1	4650849924	4651148299	298375	1067.05	660.12	31	45	-1	11.64	11.64
Saccade L	1	2	4651148299	4651227766	79467	1058.79	628.09	1062.39	172.61	17.95	1076.01	1.00	225.89	23144.10	-3861.94	5667.10
Saccade R	1	2	4651148299	4651227766	79467	1058.79	628.09	1062.39	172.61	17.95	1076.01	1.00	225.89	23144.10	-3861.94	5667.10
Blink L	1	1	4651227766	4651406943	179177
Blink R	1	1	4651227766	4651406943	179177
Saccade L	1	3	4651406943	4651426783	19840	1090.84	236.17	1090.47	347.36	1.17	112.47	1.00	59.18	1001.59	-2511.15	1375.23
Saccade R	1	3	4651406943	4651426783	19840	1090.84	236.17	1090.47	347.36	1.17	112.47	1.00	59.18	1001.59	-2511.15	1375.23
Fixation L	1	2	4651426783	4652739434	1312651	1090.56	396.09	36	59	-1	11.39	11.39
Fixation R	1	2	4651426783	4652739434	1312651	1090.56	396.09	36	59	-1	11.54	11.54
