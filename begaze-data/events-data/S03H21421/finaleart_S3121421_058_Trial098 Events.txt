[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S3121421-finaleart-1.idf
Date:	27.03.2019 15:33:56
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S3121421
Description:	Jean Jouve

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	4345315252	# Message: void.jpg
Fixation L	1	1	4345324384	4345602871	278487	1534.10	989.01	45	51	-1	10.28	10.28
Fixation R	1	1	4345324384	4345602871	278487	1534.10	989.01	45	51	-1	9.22	9.22
Saccade L	1	1	4345602871	4345622738	19867	1549.96	1026.35	1544.04	1029.98	0.49	41.95	0.00	24.46	733.57	-343.70	472.84
Saccade R	1	1	4345602871	4345622738	19867	1549.96	1026.35	1544.04	1029.98	0.49	41.95	0.00	24.46	733.57	-343.70	472.84
Fixation L	1	2	4345622738	4345781853	159115	1547.30	1029.88	10	53	-1	10.13	10.13
Fixation R	1	2	4345622738	4345781853	159115	1547.30	1029.88	10	53	-1	9.17	9.17
Saccade L	1	2	4345781853	4345801733	19880	1543.14	1030.84	1481.44	1010.00	1.18	144.49	1.00	59.11	3193.04	292.48	1633.16
Saccade R	1	2	4345781853	4345801733	19880	1543.14	1030.84	1481.44	1010.00	1.18	144.49	1.00	59.11	3193.04	292.48	1633.16
Blink L	1	1	4345801733	4346239338	437605
Blink R	1	1	4345801733	4346239338	437605
Fixation L	1	3	4346239338	4347373027	1133689	967.60	339.03	23	72	-1	10.05	10.05
Fixation R	1	3	4346239338	4347373027	1133689	967.60	339.03	23	72	-1	10.09	10.09
