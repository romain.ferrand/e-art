[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S3121421-finaleart-1.idf
Date:	27.03.2019 15:34:16
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S3121421
Description:	Jean Jouve

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	5611610609	# Message: void.jpg
Fixation L	1	1	5611613927	5613027679	1413752	1292.16	495.49	32	64	-1	10.89	10.89
Fixation R	1	1	5611613927	5613027679	1413752	1292.16	495.49	32	64	-1	11.11	11.11
Saccade L	1	1	5613027679	5613047697	20018	1273.67	521.54	1071.44	516.25	1.25	204.57	1.00	62.45	5077.17	-4809.45	3474.26
Saccade R	1	1	5613027679	5613047697	20018	1273.67	521.54	1071.44	516.25	1.25	204.57	1.00	62.45	5077.17	-4809.45	3474.26
Fixation L	1	2	5613047697	5613605141	557444	1022.46	526.52	57	37	-1	12.26	12.26
Fixation R	1	2	5613047697	5613605141	557444	1022.46	526.52	57	37	-1	12.75	12.75
