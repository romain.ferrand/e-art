[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S3121421-finaleart-1.idf
Date:	27.03.2019 15:33:56
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S3121421
Description:	Jean Jouve

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	4333094442	# Message: void.jpg
Fixation L	1	1	4333112501	4333530098	417597	595.23	175.09	46	21	-1	9.62	9.62
Fixation R	1	1	4333112501	4333530098	417597	595.23	175.09	46	21	-1	10.76	10.76
Saccade L	1	1	4333530098	4333788716	258618	628.97	190.63	1090.94	635.24	13.72	132.29	0.08	53.06	3070.56	-2917.62	1170.76
Saccade R	1	1	4333530098	4333788716	258618	628.97	190.63	1090.94	635.24	13.72	132.29	0.08	53.06	3070.56	-2917.62	1170.76
Fixation L	1	2	4333788716	4334624038	835322	1085.83	655.31	31	64	-1	11.04	11.04
Fixation R	1	2	4333788716	4334624038	835322	1085.83	655.31	31	64	-1	11.76	11.76
Saccade L	1	2	4334624038	4334683659	59621	1077.06	659.30	1039.84	355.36	3.61	149.74	0.33	60.61	3671.43	-2464.80	2083.30
Saccade R	1	2	4334624038	4334683659	59621	1077.06	659.30	1039.84	355.36	3.61	149.74	0.33	60.61	3671.43	-2464.80	2083.30
Fixation L	1	3	4334683659	4335081504	397845	1007.63	361.41	45	25	-1	11.34	11.34
Fixation R	1	3	4334683659	4335081504	397845	1007.63	361.41	45	25	-1	11.56	11.56
