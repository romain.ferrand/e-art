[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S3121421-finaleart-1.idf
Date:	27.03.2019 15:34:12
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S3121421
Description:	Jean Jouve

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	5506821972	# Message: void.jpg
Saccade L	1	1	5506833723	5506893350	59627	343.15	710.39	452.15	470.13	3.75	141.27	0.67	62.94	3531.74	-3217.69	2510.20
Saccade R	1	1	5506833723	5506893350	59627	343.15	710.39	452.15	470.13	3.75	141.27	0.67	62.94	3531.74	-3217.69	2510.20
Fixation L	1	1	5506893350	5507211573	318223	462.14	487.70	16	32	-1	10.60	10.60
Fixation R	1	1	5506893350	5507211573	318223	462.14	487.70	16	32	-1	10.03	10.03
Saccade L	1	2	5507211573	5507231445	19872	466.86	485.68	516.82	465.56	0.69	72.27	1.00	34.57	1599.39	-815.49	1232.01
Saccade R	1	2	5507211573	5507231445	19872	466.86	485.68	516.82	465.56	0.69	72.27	1.00	34.57	1599.39	-815.49	1232.01
Blink L	1	1	5507231445	5507549677	318232
Blink R	1	1	5507231445	5507549677	318232
Fixation L	1	2	5507549677	5508822610	1272933	899.78	267.12	25	36	-1	10.71	10.71
Fixation R	1	2	5507549677	5508822610	1272933	899.78	267.12	25	36	-1	10.60	10.60
