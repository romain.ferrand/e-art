[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S3121421-finaleart-1.idf
Date:	27.03.2019 15:33:55
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S3121421
Description:	Jean Jouve

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	4308598299	# Message: void.jpg
Saccade L	1	1	4308608976	4308708477	99501	1227.88	696.93	1488.20	830.54	5.85	160.60	1.00	58.81	3508.84	-3850.51	1878.23
Saccade R	1	1	4308608976	4308708477	99501	1227.88	696.93	1488.20	830.54	5.85	160.60	1.00	58.81	3508.84	-3850.51	1878.23
Fixation L	1	1	4308708477	4308827849	119372	1484.47	880.86	27	65	-1	11.72	11.72
Fixation R	1	1	4308708477	4308827849	119372	1484.47	880.86	27	65	-1	9.90	9.90
Saccade L	1	2	4308827849	4308847725	19876	1466.66	895.90	1444.26	900.68	0.32	23.17	1.00	16.03	286.01	-352.36	219.83
Saccade R	1	2	4308827849	4308847725	19876	1466.66	895.90	1444.26	900.68	0.32	23.17	1.00	16.03	286.01	-352.36	219.83
Fixation L	1	2	4308847725	4309404587	556862	1445.39	920.75	44	42	-1	11.90	11.90
Fixation R	1	2	4308847725	4309404587	556862	1445.39	920.75	44	42	-1	9.97	9.97
Saccade L	1	3	4309404587	4309504039	99452	1444.54	916.89	1045.87	319.68	10.47	208.79	0.40	105.27	4820.22	-3840.51	2640.25
Saccade R	1	3	4309404587	4309504039	99452	1444.54	916.89	1045.87	319.68	10.47	208.79	0.40	105.27	4820.22	-3840.51	2640.25
Fixation L	1	3	4309504039	4310597864	1093825	1008.85	298.84	46	32	-1	11.51	11.51
Fixation R	1	3	4309504039	4310597864	1093825	1008.85	298.84	46	32	-1	11.37	11.37
