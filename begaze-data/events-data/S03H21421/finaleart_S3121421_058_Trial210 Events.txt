[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S3121421-finaleart-1.idf
Date:	27.03.2019 15:34:06
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S3121421
Description:	Jean Jouve

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	5000470761	# Message: void.jpg
Saccade L	1	1	5000481308	5000540927	59619	959.71	931.12	966.60	736.28	2.65	153.67	1.00	44.39	3756.80	-3687.12	1781.64
Saccade R	1	1	5000481308	5000540927	59619	959.71	931.12	966.60	736.28	2.65	153.67	1.00	44.39	3756.80	-3687.12	1781.64
Fixation L	1	1	5000540927	5002430431	1889504	966.92	740.51	24	71	-1	10.37	10.37
Fixation R	1	1	5000540927	5002430431	1889504	966.92	740.51	24	71	-1	10.90	10.90
Saccade L	1	2	5002430431	5002470214	39783	966.41	713.62	966.59	699.70	0.40	21.71	0.00	10.11	103.86	-410.74	171.53
Saccade R	1	2	5002430431	5002470214	39783	966.41	713.62	966.59	699.70	0.40	21.71	0.00	10.11	103.86	-410.74	171.53
