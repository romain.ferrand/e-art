[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S3121421-finaleart-1.idf
Date:	27.03.2019 15:33:58
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S3121421
Description:	Jean Jouve

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	4541995835	# Message: void.jpg
Saccade L	1	1	4541999649	4542079251	79602	612.21	325.84	594.55	777.84	6.76	189.83	0.25	84.95	3393.34	-3203.29	1827.24
Saccade R	1	1	4541999649	4542079251	79602	612.21	325.84	594.55	777.84	6.76	189.83	0.25	84.95	3393.34	-3203.29	1827.24
Fixation L	1	1	4542079251	4542616473	537222	612.81	779.23	28	44	-1	13.76	13.76
Fixation R	1	1	4542079251	4542616473	537222	612.81	779.23	28	44	-1	14.12	14.12
Saccade L	1	2	4542616473	4542656253	39780	612.82	793.87	821.70	695.50	3.30	168.87	1.00	82.88	3104.48	-783.06	1907.46
Saccade R	1	2	4542616473	4542656253	39780	612.82	793.87	821.70	695.50	3.30	168.87	1.00	82.88	3104.48	-783.06	1907.46
Blink L	1	1	4542656253	4542994580	338327
Blink R	1	1	4542656253	4542994580	338327
Fixation L	1	2	4542994580	4543989380	994800	1015.10	328.52	10	52	-1	13.21	13.21
Fixation R	1	2	4542994580	4543989380	994800	1015.10	328.52	10	52	-1	13.33	13.33
