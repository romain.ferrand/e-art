[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S3121421-finaleart-1.idf
Date:	27.03.2019 15:33:53
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S3121421
Description:	Jean Jouve

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	4247245324	# Message: void.jpg
Saccade L	1	1	4247263938	4247283809	19871	849.67	324.87	956.84	232.67	1.09	142.99	1.00	54.89	542.35	-3082.53	1208.29
Saccade R	1	1	4247263938	4247283809	19871	849.67	324.87	956.84	232.67	1.09	142.99	1.00	54.89	542.35	-3082.53	1208.29
Fixation L	1	1	4247283809	4247562690	278881	989.52	240.60	51	20	-1	12.07	12.07
Fixation R	1	1	4247283809	4247562690	278881	989.52	240.60	51	20	-1	12.55	12.55
Saccade L	1	2	4247562690	4247582542	19852	949.41	236.11	913.48	227.17	0.47	48.36	0.00	23.82	856.11	-1052.62	757.15
Saccade R	1	2	4247562690	4247582542	19852	949.41	236.11	913.48	227.17	0.47	48.36	0.00	23.82	856.11	-1052.62	757.15
Fixation L	1	2	4247582542	4248856970	1274428	889.18	213.79	33	58	-1	11.08	11.08
Fixation R	1	2	4247582542	4248856970	1274428	889.18	213.79	33	58	-1	11.44	11.44
Saccade L	1	3	4248856970	4248916721	59751	896.70	189.39	1055.86	451.44	3.47	144.77	0.67	58.04	3421.31	-3186.45	2516.25
Saccade R	1	3	4248856970	4248916721	59751	896.70	189.39	1055.86	451.44	3.47	144.77	0.67	58.04	3421.31	-3186.45	2516.25
Fixation L	1	3	4248916721	4249235317	318596	1082.86	427.93	33	40	-1	11.93	11.93
Fixation R	1	3	4248916721	4249235317	318596	1082.86	427.93	33	40	-1	12.20	12.20
