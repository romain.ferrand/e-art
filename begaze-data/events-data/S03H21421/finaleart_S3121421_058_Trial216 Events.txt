[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S3121421-finaleart-1.idf
Date:	27.03.2019 15:34:07
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S3121421
Description:	Jean Jouve

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	5230909685	# Message: void.jpg
Saccade L	1	1	5230911283	5230971016	59733	1252.91	635.92	1090.37	643.39	2.06	156.39	1.00	34.55	3907.66	-3802.80	1584.37
Saccade R	1	1	5230911283	5230971016	59733	1252.91	635.92	1090.37	643.39	2.06	156.39	1.00	34.55	3907.66	-3802.80	1584.37
Fixation L	1	1	5230971016	5231408735	437719	1100.38	653.60	17	33	-1	11.57	11.57
Fixation R	1	1	5230971016	5231408735	437719	1100.38	653.60	17	33	-1	10.90	10.90
Blink L	1	1	5231408735	5231547973	139238
Blink R	1	1	5231408735	5231547973	139238
Blink L	1	2	5231567967	5231647481	79514
Blink R	1	2	5231567967	5231647481	79514
Fixation L	1	2	5231647481	5232940893	1293412	843.93	507.69	14	38	-1	10.02	10.02
Fixation R	1	2	5231647481	5232940893	1293412	843.93	507.69	14	38	-1	9.45	9.45
