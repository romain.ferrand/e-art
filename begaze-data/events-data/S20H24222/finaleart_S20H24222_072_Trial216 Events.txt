[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S20H24222-finaleart-1.idf
Date:	27.03.2019 15:53:37
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S20H24222
Description:	Hugo Brument

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	12100238269	# Message: void.jpg
Fixation L	1	1	12100241714	12100659312	417598	1172.37	427.69	19	37	-1	11.20	11.20
Fixation R	1	1	12100241714	12100659312	417598	1172.37	427.69	19	37	-1	11.52	11.52
Blink L	1	1	12100659312	12100838318	179006
Blink R	1	1	12100659312	12100838318	179006
Fixation L	1	2	12100838318	12101693645	855327	1023.40	558.79	43	48	-1	11.88	11.88
Fixation R	1	2	12100838318	12101693645	855327	1023.40	558.79	43	48	-1	11.48	11.48
Saccade L	1	1	12101693645	12101713520	19875	989.38	535.91	862.64	565.19	0.95	131.56	1.00	47.86	3228.28	-2461.39	1966.39
Saccade R	1	1	12101693645	12101713520	19875	989.38	535.91	862.64	565.19	0.95	131.56	1.00	47.86	3228.28	-2461.39	1966.39
Fixation L	1	3	12101713520	12102111379	397859	832.08	590.08	41	53	-1	11.94	11.94
Fixation R	1	3	12101713520	12102111379	397859	832.08	590.08	41	53	-1	11.68	11.68
Saccade L	1	2	12102111379	12102131247	19868	861.99	602.22	869.39	600.18	0.12	7.77	1.00	6.11	38.97	-120.79	83.53
Saccade R	1	2	12102111379	12102131247	19868	861.99	602.22	869.39	600.18	0.12	7.77	1.00	6.11	38.97	-120.79	83.53
Fixation L	1	4	12102131247	12102230619	99372	875.44	603.52	12	9	-1	12.21	12.21
Fixation R	1	4	12102131247	12102230619	99372	875.44	603.52	12	9	-1	12.13	12.13
