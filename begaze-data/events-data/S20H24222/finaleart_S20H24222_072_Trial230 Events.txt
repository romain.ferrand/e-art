[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S20H24222-finaleart-1.idf
Date:	27.03.2019 15:53:43
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S20H24222
Description:	Hugo Brument

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	12143108635	# Message: void.jpg
Fixation L	1	1	12143124712	12143542440	417728	839.19	316.96	8	31	-1	14.13	14.13
Fixation R	1	1	12143124712	12143542440	417728	839.19	316.96	8	31	-1	13.87	13.87
Blink L	1	1	12143542440	12143701566	159126
Blink R	1	1	12143542440	12143701566	159126
Fixation L	1	2	12143701566	12144437410	735844	991.86	508.67	28	44	-1	14.60	14.60
Fixation R	1	2	12143701566	12144437410	735844	991.86	508.67	28	44	-1	13.94	13.94
Saccade L	1	1	12144437410	12144457283	19873	990.18	534.05	919.97	532.18	0.48	71.05	1.00	24.11	1700.11	-1564.69	1127.79
Saccade R	1	1	12144437410	12144457283	19873	990.18	534.05	919.97	532.18	0.48	71.05	1.00	24.11	1700.11	-1564.69	1127.79
Fixation L	1	3	12144457283	12145093752	636469	891.15	519.35	35	41	-1	13.72	13.72
Fixation R	1	3	12144457283	12145093752	636469	891.15	519.35	35	41	-1	13.56	13.56
