[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S20H24222-finaleart-1.idf
Date:	27.03.2019 15:52:06
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S20H24222
Description:	Hugo Brument

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	11026876707	# Message: void.jpg
Fixation L	1	1	11026888622	11027465463	576841	528.87	394.57	20	62	-1	10.18	10.18
Fixation R	1	1	11026888622	11027465463	576841	528.87	394.57	20	62	-1	10.72	10.72
Blink L	1	1	11027465463	11027664330	198867
Blink R	1	1	11027465463	11027664330	198867
Fixation L	1	2	11027664330	11028261053	596723	1167.04	659.45	29	29	-1	13.80	13.80
Fixation R	1	2	11027664330	11028261053	596723	1167.04	659.45	29	29	-1	14.21	14.21
Saccade L	1	1	11028261053	11028300922	39869	1152.79	656.75	856.84	572.67	2.81	211.52	0.50	70.50	5135.70	-5098.13	3653.48
Saccade R	1	1	11028261053	11028300922	39869	1152.79	656.75	856.84	572.67	2.81	211.52	0.50	70.50	5135.70	-5098.13	3653.48
Fixation L	1	3	11028300922	11028857781	556859	858.77	578.06	13	38	-1	13.38	13.38
Fixation R	1	3	11028300922	11028857781	556859	858.77	578.06	13	38	-1	14.32	14.32
