[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S20H24222-finaleart-1.idf
Date:	27.03.2019 15:54:07
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S20H24222
Description:	Hugo Brument

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	12339949202	# Message: void.jpg
Saccade L	1	1	12339958527	12340018276	59749	1133.54	846.06	1327.92	938.02	2.65	218.55	1.00	44.33	5437.50	-5356.35	2164.81
Saccade R	1	1	12339958527	12340018276	59749	1133.54	846.06	1327.92	938.02	2.65	218.55	1.00	44.33	5437.50	-5356.35	2164.81
Fixation L	1	1	12340018276	12340396119	377843	1330.10	932.52	8	13	-1	14.70	14.70
Fixation R	1	1	12340018276	12340396119	377843	1330.10	932.52	8	13	-1	14.58	14.58
Blink L	1	1	12340396119	12340614867	218748
Blink R	1	1	12340396119	12340614867	218748
Fixation L	1	2	12340614867	12341947558	1332691	909.91	588.12	65	34	-1	12.68	12.68
Fixation R	1	2	12340614867	12341947558	1332691	909.91	588.12	65	34	-1	13.30	13.30
