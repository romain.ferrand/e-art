[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S20H24222-finaleart-1.idf
Date:	27.03.2019 15:53:08
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S20H24222
Description:	Hugo Brument

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	11723536517	# Message: void.jpg
Fixation L	1	1	11723546136	11724003601	457465	1473.02	253.61	42	48	-1	9.01	9.01
Fixation R	1	1	11723546136	11724003601	457465	1473.02	253.61	42	48	-1	9.04	9.04
Blink L	1	1	11724003601	11724242369	238768
Blink R	1	1	11724003601	11724242369	238768
Fixation L	1	2	11724242369	11724779323	536954	1127.98	461.91	36	60	-1	11.40	11.40
Fixation R	1	2	11724242369	11724779323	536954	1127.98	461.91	36	60	-1	11.72	11.72
Saccade L	1	1	11724779323	11724799191	19868	1104.91	481.06	931.74	490.44	1.03	175.39	1.00	51.97	4326.84	-4245.85	2997.34
Saccade R	1	1	11724779323	11724799191	19868	1104.91	481.06	931.74	490.44	1.03	175.39	1.00	51.97	4326.84	-4245.85	2997.34
Fixation L	1	3	11724799191	11725535160	735969	914.47	509.85	30	38	-1	11.63	11.63
Fixation R	1	3	11724799191	11725535160	735969	914.47	509.85	30	38	-1	12.25	12.25
