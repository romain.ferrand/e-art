[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S12H22222-finaleart-1.idf
Date:	27.03.2019 15:40:57
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S12H22222
Description:	aurele barriere

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	23932565325	# Message: void.jpg
Fixation L	1	1	23932574731	23932694106	119375	774.25	922.66	1	2	-1	14.08	14.08
Fixation R	1	1	23932574731	23932694106	119375	774.25	922.66	1	2	-1	13.36	13.36
Saccade L	1	1	23932694106	23932753854	59748	773.05	923.01	1228.00	401.27	7.47	257.89	0.67	125.02	6407.89	-5707.27	4634.25
Saccade R	1	1	23932694106	23932753854	59748	773.05	923.01	1228.00	401.27	7.47	257.89	0.67	125.02	6407.89	-5707.27	4634.25
Fixation L	1	2	23932753854	23933609037	855183	1220.46	427.42	22	59	-1	13.74	13.74
Fixation R	1	2	23932753854	23933609037	855183	1220.46	427.42	22	59	-1	13.02	13.02
Saccade L	1	2	23933609037	23933648791	39754	1205.95	460.61	1121.42	461.79	1.00	59.14	1.00	25.08	1436.89	-1099.36	762.71
Saccade R	1	2	23933609037	23933648791	39754	1205.95	460.61	1121.42	461.79	1.00	59.14	1.00	25.08	1436.89	-1099.36	762.71
Fixation L	1	3	23933648791	23934504131	855340	1082.41	476.08	61	35	-1	14.42	14.42
Fixation R	1	3	23933648791	23934504131	855340	1082.41	476.08	61	35	-1	13.49	13.49
Saccade L	1	3	23934504131	23934563751	59620	1059.67	496.83	1041.22	487.39	0.88	25.68	0.00	14.75	25.84	-409.92	179.21
Saccade R	1	3	23934504131	23934563751	59620	1059.67	496.83	1041.22	487.39	0.88	25.68	0.00	14.75	25.84	-409.92	179.21
