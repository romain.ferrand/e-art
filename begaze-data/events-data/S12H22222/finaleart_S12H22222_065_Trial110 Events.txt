[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S12H22222-finaleart-1.idf
Date:	27.03.2019 15:40:17
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S12H22222
Description:	aurele barriere

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	23366976265	# Message: void.jpg
Fixation L	1	1	23366978572	23367197423	218851	968.28	464.31	53	39	-1	13.88	13.88
Fixation R	1	1	23366978572	23367197423	218851	968.28	464.31	53	39	-1	12.89	12.89
Saccade L	1	1	23367197423	23367217328	19905	1000.71	439.81	993.14	431.07	0.30	22.26	0.00	15.18	-11.42	-191.45	115.97
Saccade R	1	1	23367197423	23367217328	19905	1000.71	439.81	993.14	431.07	0.30	22.26	0.00	15.18	-11.42	-191.45	115.97
Fixation L	1	2	23367217328	23368967595	1750267	1021.71	452.27	39	54	-1	15.22	15.22
Fixation R	1	2	23367217328	23368967595	1750267	1021.71	452.27	39	54	-1	14.29	14.29
