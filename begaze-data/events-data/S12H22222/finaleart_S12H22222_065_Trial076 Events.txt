[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S12H22222-finaleart-1.idf
Date:	27.03.2019 15:40:08
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S12H22222
Description:	aurele barriere

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	23150212825	# Message: void.jpg
Fixation L	1	1	23150216255	23150872589	656334	1146.40	518.11	15	12	-1	14.61	14.61
Fixation R	1	1	23150216255	23150872589	656334	1146.40	518.11	15	12	-1	13.62	13.62
Saccade L	1	1	23150872589	23150892451	19862	1136.73	521.14	989.85	504.76	0.96	149.48	1.00	48.33	3721.89	-3495.92	2506.88
Saccade R	1	1	23150872589	23150892451	19862	1136.73	521.14	989.85	504.76	0.96	149.48	1.00	48.33	3721.89	-3495.92	2506.88
Fixation L	1	2	23150892451	23152205278	1312827	946.53	498.92	53	34	-1	15.22	15.22
Fixation R	1	2	23150892451	23152205278	1312827	946.53	498.92	53	34	-1	14.17	14.17
