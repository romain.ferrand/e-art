[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S19F21442-finaleart-1.idf
Date:	27.03.2019 15:50:53
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S19F21442
Description:	Juliette Veuillez

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	6712983480	# Message: void.jpg
Fixation L	1	1	6713000787	6714751088	1750301	865.62	469.02	42	55	-1	11.37	11.37
Fixation R	1	1	6713000787	6714751088	1750301	865.62	469.02	42	55	-1	12.38	12.38
Saccade L	1	1	6714751088	6714770955	19867	869.34	492.10	873.28	500.97	0.18	15.11	1.00	9.16	305.07	26.00	187.19
Saccade R	1	1	6714751088	6714770955	19867	869.34	492.10	873.28	500.97	0.18	15.11	1.00	9.16	305.07	26.00	187.19
Fixation L	1	2	6714770955	6714969828	198873	873.14	500.06	5	22	-1	10.80	10.80
Fixation R	1	2	6714770955	6714969828	198873	873.14	500.06	5	22	-1	11.97	11.97
