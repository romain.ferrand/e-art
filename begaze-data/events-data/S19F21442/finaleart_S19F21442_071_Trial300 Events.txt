[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S19F21442-finaleart-1.idf
Date:	27.03.2019 15:51:59
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S19F21442
Description:	Juliette Veuillez

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	8323171910	# Message: void.jpg
Saccade L	1	1	8323183324	8323282819	99495	1550.34	1026.34	1111.42	467.08	10.71	321.52	0.60	107.66	8014.96	-4767.01	3668.85
Saccade R	1	1	8323183324	8323282819	99495	1550.34	1026.34	1111.42	467.08	10.71	321.52	0.60	107.66	8014.96	-4767.01	3668.85
Fixation L	1	1	8323282819	8323422057	139238	1106.84	474.41	10	38	-1	10.60	10.60
Fixation R	1	1	8323282819	8323422057	139238	1106.84	474.41	10	38	-1	11.51	11.51
Saccade L	1	2	8323422057	8323481680	59623	1103.50	497.55	690.99	439.25	4.48	253.34	0.67	75.13	6040.85	-6029.14	3283.41
Saccade R	1	2	8323422057	8323481680	59623	1103.50	497.55	690.99	439.25	4.48	253.34	0.67	75.13	6040.85	-6029.14	3283.41
Fixation L	1	2	8323481680	8324734754	1253074	703.26	462.57	22	77	-1	10.72	10.72
Fixation R	1	2	8323481680	8324734754	1253074	703.26	462.57	22	77	-1	12.54	12.54
Saccade L	1	3	8324734754	8324754631	19877	710.82	478.45	714.07	489.64	0.14	11.79	1.00	7.00	154.77	-217.32	142.94
Saccade R	1	3	8324734754	8324754631	19877	710.82	478.45	714.07	489.64	0.14	11.79	1.00	7.00	154.77	-217.32	142.94
Fixation L	1	3	8324754631	8325152481	397850	717.32	491.19	61	16	-1	11.95	11.95
Fixation R	1	3	8324754631	8325152481	397850	717.32	491.19	61	16	-1	13.69	13.69
