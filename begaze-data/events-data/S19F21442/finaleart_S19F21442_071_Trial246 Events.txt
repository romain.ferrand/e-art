[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S19F21442-finaleart-1.idf
Date:	27.03.2019 15:51:37
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S19F21442
Description:	Juliette Veuillez

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	7726088290	# Message: void.jpg
Fixation L	1	1	7726088358	7726207741	119383	1417.90	974.09	5	8	-1	11.56	11.56
Fixation R	1	1	7726088358	7726207741	119383	1417.90	974.09	5	8	-1	11.58	11.58
Saccade L	1	1	7726207741	7726247488	39747	1418.99	977.45	1042.95	1010.90	3.30	213.53	1.00	83.05	5265.27	-5267.72	4553.31
Saccade R	1	1	7726207741	7726247488	39747	1418.99	977.45	1042.95	1010.90	3.30	213.53	1.00	83.05	5265.27	-5267.72	4553.31
Fixation L	1	2	7726247488	7727599910	1352422	1040.42	1009.83	23	64	-1	11.72	11.72
Fixation R	1	2	7726247488	7727599910	1352422	1040.42	1009.83	23	64	-1	11.98	11.98
Saccade L	1	2	7727599910	7727679533	79623	1035.18	1032.14	830.88	441.60	7.41	236.87	0.75	93.09	4073.95	-5728.61	3256.32
Saccade R	1	2	7727599910	7727679533	79623	1035.18	1032.14	830.88	441.60	7.41	236.87	0.75	93.09	4073.95	-5728.61	3256.32
Fixation L	1	3	7727679533	7728077390	397857	823.37	456.27	13	39	-1	11.40	11.40
Fixation R	1	3	7727679533	7728077390	397857	823.37	456.27	13	39	-1	12.69	12.69
