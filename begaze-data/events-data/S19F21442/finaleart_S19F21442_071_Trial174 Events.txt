[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S19F21442-finaleart-1.idf
Date:	27.03.2019 15:51:08
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S19F21442
Description:	Juliette Veuillez

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	7134585545	# Message: void.jpg
Fixation L	1	1	7134589787	7134828510	238723	607.98	186.06	14	34	-1	11.13	11.13
Fixation R	1	1	7134589787	7134828510	238723	607.98	186.06	14	34	-1	11.23	11.23
Saccade L	1	1	7134828510	7134868253	39743	600.24	170.31	613.68	510.31	2.90	239.40	1.00	72.88	5943.22	-5689.37	4102.06
Saccade R	1	1	7134828510	7134868253	39743	600.24	170.31	613.68	510.31	2.90	239.40	1.00	72.88	5943.22	-5689.37	4102.06
Fixation L	1	2	7134868253	7135186493	318240	627.66	525.84	27	25	-1	11.55	11.55
Fixation R	1	2	7134868253	7135186493	318240	627.66	525.84	27	25	-1	11.87	11.87
Blink L	1	1	7135186493	7135365480	178987
Blink R	1	1	7135186493	7135266130	79637
Blink R	1	2	7135266130	7135365480	99350
Fixation L	1	3	7135365480	7135783227	417747	612.07	899.85	65	33	-1	13.75	13.75
Fixation R	1	3	7135365480	7135783227	417747	612.07	899.85	65	33	-1	14.41	14.41
Saccade L	1	2	7135783227	7135803088	19861	558.98	916.58	557.33	916.25	0.15	18.93	0.00	7.40	10.12	-295.87	103.37
Saccade R	1	2	7135783227	7135803088	19861	558.98	916.58	557.33	916.25	0.15	18.93	0.00	7.40	10.12	-295.87	103.37
Fixation L	1	4	7135803088	7136578808	775720	544.60	945.31	20	37	-1	13.18	13.18
Fixation R	1	4	7135803088	7136578808	775720	544.60	945.31	20	37	-1	13.81	13.81
