[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S01A22542-finaleart-1.idf
Date:	27.03.2019 15:33:21
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S01A22542
Description:	Thibault Seve Minnaert

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	9491351130	# Message: void.jpg
Fixation L	1	1	9491367833	9491467331	99498	1390.41	351.72	6	17	-1	12.37	12.37
Fixation R	1	1	9491367833	9491467331	99498	1390.41	351.72	6	17	-1	11.63	11.63
Saccade L	1	1	9491467331	9491507246	39915	1388.09	353.56	1292.65	704.79	3.18	184.90	0.50	79.62	4490.32	-4170.70	4307.69
Saccade R	1	1	9491467331	9491507246	39915	1388.09	353.56	1292.65	704.79	3.18	184.90	0.50	79.62	4490.32	-4170.70	4307.69
Fixation L	1	2	9491507246	9491924813	417567	1280.06	739.23	16	45	-1	12.79	12.79
Fixation R	1	2	9491507246	9491924813	417567	1280.06	739.23	16	45	-1	11.84	11.84
Blink L	1	1	9491924813	9492183303	258490
Blink R	1	1	9491924813	9492183303	258490
Fixation L	1	3	9492183303	9493118128	934825	934.67	556.67	23	23	-1	13.46	13.46
Fixation R	1	3	9492183303	9493118128	934825	934.67	556.67	23	23	-1	13.18	13.18
Saccade L	1	2	9493118128	9493138129	20001	935.42	569.38	826.08	565.29	0.62	110.67	1.00	30.94	2703.37	-2658.02	1835.62
Saccade R	1	2	9493118128	9493138129	20001	935.42	569.38	826.08	565.29	0.62	110.67	1.00	30.94	2703.37	-2658.02	1835.62
Fixation L	1	4	9493138129	9493336978	198849	805.84	561.45	32	8	-1	13.35	13.35
Fixation R	1	4	9493138129	9493336978	198849	805.84	561.45	32	8	-1	13.49	13.49
