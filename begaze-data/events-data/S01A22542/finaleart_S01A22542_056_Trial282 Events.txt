[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S01A22542-finaleart-1.idf
Date:	27.03.2019 15:33:25
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S01A22542
Description:	Thibault Seve Minnaert

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	10031659048	# Message: void.jpg
Saccade L	1	1	10031666493	10031746001	79508	1089.42	700.21	662.99	704.81	6.15	305.65	0.75	77.38	7409.18	-7277.47	3483.71
Saccade R	1	1	10031666493	10031746001	79508	1089.42	700.21	662.99	704.81	6.15	305.65	0.75	77.38	7409.18	-7277.47	3483.71
Fixation L	1	1	10031746001	10032263124	517123	662.82	753.35	11	65	-1	12.14	12.14
Fixation R	1	1	10031746001	10032263124	517123	662.82	753.35	11	65	-1	12.55	12.55
Blink L	1	1	10032263124	10032561569	298445
Blink R	1	1	10032263124	10032561569	298445
Fixation L	1	2	10032561569	10033158162	596593	946.43	591.25	32	23	-1	12.47	12.47
Fixation R	1	2	10032561569	10033158162	596593	946.43	591.25	32	23	-1	12.59	12.59
Saccade L	1	2	10033158162	10033178169	20007	964.93	596.08	1099.72	596.01	0.86	136.33	1.00	42.82	3277.20	-2980.12	2113.83
Saccade R	1	2	10033158162	10033178169	20007	964.93	596.08	1099.72	596.01	0.86	136.33	1.00	42.82	3277.20	-2980.12	2113.83
Fixation L	1	3	10033178169	10033655542	477373	1121.07	571.52	25	36	-1	12.52	12.52
Fixation R	1	3	10033178169	10033655542	477373	1121.07	571.52	25	36	-1	12.63	12.63
