[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S02123220-finaleart-1.idf
Date:	27.03.2019 15:33:30
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S02123220
Description:	David Xu

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	505797706	# Message: void.jpg
Saccade L	1	1	505803769	505863389	59620	588.39	350.41	1048.68	449.15	6.32	315.66	1.00	106.00	7754.78	-7563.48	4792.15
Saccade R	1	1	505803769	505863389	59620	588.39	350.41	1048.68	449.15	6.32	315.66	1.00	106.00	7754.78	-7563.48	4792.15
Fixation L	1	1	505863389	506281102	417713	1060.07	487.85	16	47	-1	19.19	19.19
Fixation R	1	1	505863389	506281102	417713	1060.07	487.85	16	47	-1	17.03	17.03
Saccade L	1	2	506281102	506360629	79527	1064.30	491.75	864.88	463.43	2.85	101.31	0.50	35.86	2946.48	-2474.89	1910.69
Saccade R	1	2	506281102	506360629	79527	1064.30	491.75	864.88	463.43	2.85	101.31	0.50	35.86	2946.48	-2474.89	1910.69
Blink L	1	1	506360629	506639091	278462
Blink R	1	1	506360629	506460110	99481
Blink R	1	2	506479991	506639091	159100
Saccade L	1	3	506639091	506738578	99487	861.59	451.49	871.96	514.93	5.71	134.70	1.00	57.40	2718.04	-3071.10	1763.31
Saccade R	1	3	506639091	506738578	99487	861.59	451.49	871.96	514.93	5.71	134.70	1.00	57.40	2718.04	-3071.10	1763.31
Fixation L	1	2	506738578	507792762	1054184	884.73	483.54	28	55	-1	19.33	19.33
Fixation R	1	2	506738578	507792762	1054184	884.73	483.54	28	55	-1	17.87	17.87
