[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S02123220-finaleart-1.idf
Date:	27.03.2019 15:33:46
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S02123220
Description:	David Xu

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	2237553059	# Message: void.jpg
Fixation L	1	1	2237555293	2238331027	775734	1052.51	485.53	66	33	-1	14.39	14.39
Fixation R	1	1	2237555293	2238331027	775734	1052.51	485.53	66	33	-1	11.97	11.97
Saccade L	1	1	2238331027	2238350866	19839	1074.91	494.47	1076.36	494.01	0.05	3.77	1.00	2.62	44.20	-28.19	34.59
Saccade R	1	1	2238331027	2238350866	19839	1074.91	494.47	1076.36	494.01	0.05	3.77	1.00	2.62	44.20	-28.19	34.59
Fixation L	1	2	2238350866	2238708852	357986	1063.71	475.19	65	29	-1	14.30	14.30
Fixation R	1	2	2238350866	2238708852	357986	1063.71	475.19	65	29	-1	11.94	11.94
Saccade L	1	2	2238708852	2238728725	19873	1011.65	466.35	1001.51	463.66	0.26	24.44	1.00	13.12	-110.33	-345.79	213.72
Saccade R	1	2	2238708852	2238728725	19873	1011.65	466.35	1001.51	463.66	0.26	24.44	1.00	13.12	-110.33	-345.79	213.72
Fixation L	1	3	2238728725	2239544289	815564	989.86	478.36	27	46	-1	15.25	15.25
Fixation R	1	3	2238728725	2239544289	815564	989.86	478.36	27	46	-1	12.67	12.67
