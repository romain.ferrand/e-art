[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S8119532-finaleart-1.idf
Date:	27.03.2019 16:00:10
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S8119532
Description:	Alexandre Drewery

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	2994969507	# Message: void.jpg
Saccade L	1	1	2994973809	2995033419	59610	822.99	649.72	1038.80	598.88	3.35	119.20	0.67	56.26	2979.94	-2665.71	2106.56
Saccade R	1	1	2994973809	2995033419	59610	822.99	649.72	1038.80	598.88	3.35	119.20	0.67	56.26	2979.94	-2665.71	2106.56
Fixation L	1	1	2995033419	2995252280	218861	1039.14	558.08	11	62	-1	15.69	15.69
Fixation R	1	1	2995033419	2995252280	218861	1039.14	558.08	11	62	-1	17.79	17.79
Saccade L	1	2	2995252280	2995279038	26758	1040.41	536.16	1039.13	500.80	0.57	35.80	1.00	21.24	593.43	-530.72	379.14
Saccade R	1	2	2995252280	2995279038	26758	1040.41	536.16	1039.13	500.80	0.57	35.80	1.00	21.24	593.43	-530.72	379.14
Fixation L	1	2	2995279038	2996743953	1464915	1029.52	508.38	22	54	-1	14.62	14.62
Fixation R	1	2	2995279038	2996743953	1464915	1029.52	508.38	22	54	-1	16.64	16.64
Saccade L	1	3	2996743953	2996823574	79621	1025.43	517.79	1023.56	443.52	3.05	68.91	1.00	38.31	1240.79	-837.20	755.46
Saccade R	1	3	2996743953	2996823574	79621	1025.43	517.79	1023.56	443.52	3.05	68.91	1.00	38.31	1240.79	-837.20	755.46
Fixation L	1	3	2996823574	2996982694	159120	1032.11	487.35	17	78	-1	15.31	15.31
Fixation R	1	3	2996823574	2996982694	159120	1032.11	487.35	17	78	-1	17.42	17.42
