[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S8119532-finaleart-1.idf
Date:	27.03.2019 15:59:40
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S8119532
Description:	Alexandre Drewery

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	2679987765	# Message: void.jpg
Fixation L	1	1	2680003408	2680142647	139239	1146.89	914.61	6	30	-1	16.01	16.01
Fixation R	1	1	2680003408	2680142647	139239	1146.89	914.61	6	30	-1	17.94	17.94
Saccade L	1	1	2680142647	2680401313	258666	1144.47	898.84	698.29	391.25	24.77	386.26	0.35	95.77	9162.28	-8800.38	4416.99
Saccade R	1	1	2680142647	2680401313	258666	1144.47	898.84	698.29	391.25	24.77	386.26	0.35	95.77	9162.28	-8800.38	4416.99
Fixation L	1	2	2680401313	2680719485	318172	690.80	371.36	29	34	-1	14.50	14.50
Fixation R	1	2	2680401313	2680719485	318172	690.80	371.36	29	34	-1	16.99	16.99
Blink L	1	1	2680719485	2680898485	179000
Blink R	1	1	2680719485	2680898485	179000
Fixation L	1	3	2680898485	2681873049	974564	772.38	455.21	24	49	-1	14.20	14.20
Fixation R	1	3	2680898485	2681873049	974564	772.38	455.21	24	49	-1	16.22	16.22
Saccade L	1	2	2681873049	2681892941	19892	779.91	436.16	820.13	447.77	0.38	42.35	1.00	18.85	776.25	-680.71	556.36
Saccade R	1	2	2681873049	2681892941	19892	779.91	436.16	820.13	447.77	0.38	42.35	1.00	18.85	776.25	-680.71	556.36
Fixation L	1	4	2681892941	2682012302	119361	830.39	449.07	16	16	-1	14.06	14.06
Fixation R	1	4	2681892941	2682012302	119361	830.39	449.07	16	16	-1	16.59	16.59
