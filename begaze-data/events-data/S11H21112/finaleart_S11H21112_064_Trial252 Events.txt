[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S11H21112-finaleart-1.idf
Date:	27.03.2019 15:39:33
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S11H21112
Description:	Clement legrand-Duchenne

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	5598854536	# Message: void.jpg
Fixation L	1	1	5598861609	5599159832	298223	1033.30	554.35	28	65	-1	11.81	11.81
Fixation R	1	1	5598861609	5599159832	298223	1033.30	554.35	28	65	-1	13.72	13.72
Saccade L	1	1	5599159832	5599179835	20003	1009.93	611.48	1007.43	622.39	0.39	41.75	0.00	19.28	54.02	-894.40	372.34
Saccade R	1	1	5599159832	5599179835	20003	1009.93	611.48	1007.43	622.39	0.39	41.75	0.00	19.28	54.02	-894.40	372.34
Fixation L	1	2	5599179835	5599418454	238619	1001.11	614.46	10	17	-1	11.78	11.78
Fixation R	1	2	5599179835	5599418454	238619	1001.11	614.46	10	17	-1	13.93	13.93
Blink L	1	1	5599418454	5599756555	338101
Blink R	1	1	5599418454	5599756555	338101
Fixation L	1	3	5599756555	5599975299	218744	1205.22	128.24	63	29	-1	12.34	12.34
Fixation R	1	3	5599756555	5599975299	218744	1205.22	128.24	63	29	-1	13.54	13.54
Saccade L	1	2	5599975299	5600572146	596847	1234.16	135.61	0.00	2.23	133.13	1756.47	0.77	223.06	41817.91	-43352.65	8896.91
Saccade R	1	2	5599975299	5600572146	596847	1234.16	135.61	0.00	2.23	133.13	1756.47	0.77	223.06	41817.91	-43352.65	8896.91
