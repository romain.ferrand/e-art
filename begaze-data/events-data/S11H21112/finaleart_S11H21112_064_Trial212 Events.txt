[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S11H21112-finaleart-1.idf
Date:	27.03.2019 15:39:23
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S11H21112
Description:	Clement legrand-Duchenne

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	5376690254	# Message: void.jpg
Fixation L	1	1	5376697937	5376976793	278856	1420.57	610.65	39	25	-1	13.46	13.46
Fixation R	1	1	5376697937	5376976793	278856	1420.57	610.65	39	25	-1	14.36	14.36
Saccade L	1	1	5376976793	5376996659	19866	1430.66	605.61	1426.61	529.70	0.44	76.89	1.00	22.26	1884.40	-1646.02	1226.08
Saccade R	1	1	5376976793	5376996659	19866	1430.66	605.61	1426.61	529.70	0.44	76.89	1.00	22.26	1884.40	-1646.02	1226.08
Fixation L	1	2	5376996659	5377235673	239014	1414.43	520.18	20	17	-1	13.11	13.11
Fixation R	1	2	5376996659	5377235673	239014	1414.43	520.18	20	17	-1	14.15	14.15
Blink L	1	1	5377235673	5377434765	199092
Saccade R	1	2	5377235673	5377992246	756573	1405.88	522.89	0.00	-5.03	536.87	1828.74	0.37	709.61	44484.02	-45174.13	20391.50
Saccade L	1	2	5377434765	5377992246	557481	1723.55	-3.85	0.00	-5.03	424.55	1828.74	0.14	761.55	44484.02	-45174.13	22894.16
Fixation L	1	3	5377992246	5378681684	689438	0.00	-2.11	0	5	-1	1.56	1.56
Fixation R	1	3	5377992246	5378681684	689438	0.00	-2.11	0	5	-1	1.76	1.76
