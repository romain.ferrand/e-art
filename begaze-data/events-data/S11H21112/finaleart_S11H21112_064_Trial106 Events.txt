[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S11H21112-finaleart-1.idf
Date:	27.03.2019 15:38:58
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S11H21112
Description:	Clement legrand-Duchenne

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	4410086892	# Message: void.jpg
Saccade L	1	1	4410102061	4410161809	59748	1291.98	387.15	1159.05	428.36	2.14	118.84	1.00	35.89	2914.15	-2863.49	1349.29
Saccade R	1	1	4410102061	4410161809	59748	1291.98	387.15	1159.05	428.36	2.14	118.84	1.00	35.89	2914.15	-2863.49	1349.29
Fixation L	1	1	4410161809	4411434752	1272943	1147.21	465.13	38	57	-1	14.76	14.76
Fixation R	1	1	4410161809	4411434752	1272943	1147.21	465.13	38	57	-1	17.04	17.04
Saccade L	1	2	4411434752	4411454622	19870	1123.10	473.76	1031.16	496.47	0.65	95.80	1.00	32.63	2276.74	-2242.25	1543.69
Saccade R	1	2	4411434752	4411454622	19870	1123.10	473.76	1031.16	496.47	0.65	95.80	1.00	32.63	2276.74	-2242.25	1543.69
Fixation L	1	2	4411454622	4412011616	556994	1033.94	500.31	16	23	-1	14.79	14.79
Fixation R	1	2	4411454622	4412011616	556994	1033.94	500.31	16	23	-1	17.29	17.29
Saccade L	1	3	4412011616	4412091086	79470	1041.23	499.06	951.18	577.30	2.12	68.02	0.25	26.64	1643.45	-1386.34	1074.35
Saccade R	1	3	4412011616	4412091086	79470	1041.23	499.06	951.18	577.30	2.12	68.02	0.25	26.64	1643.45	-1386.34	1074.35
