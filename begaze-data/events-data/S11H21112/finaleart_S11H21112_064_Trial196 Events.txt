[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S11H21112-finaleart-1.idf
Date:	27.03.2019 15:39:19
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S11H21112
Description:	Clement legrand-Duchenne

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	5327846311	# Message: void.jpg
Fixation L	1	1	5327858197	5327997681	139484	1129.75	573.31	2	10	-1	13.47	13.47
Fixation R	1	1	5327858197	5327997681	139484	1129.75	573.31	2	10	-1	15.49	15.49
Saccade L	1	1	5327997681	5328037461	39780	1131.13	574.43	1393.09	615.83	2.28	189.46	0.50	57.24	4728.79	-4372.99	3219.41
Saccade R	1	1	5327997681	5328037461	39780	1131.13	574.43	1393.09	615.83	2.28	189.46	0.50	57.24	4728.79	-4372.99	3219.41
Fixation L	1	2	5328037461	5328336039	298578	1395.42	653.10	16	55	-1	13.09	13.09
Fixation R	1	2	5328037461	5328336039	298578	1395.42	653.10	16	55	-1	14.69	14.69
Blink L	1	1	5328336039	5328594898	258859
Blink R	1	1	5328336039	5328594898	258859
Blink L	1	2	5328614898	5329052628	437730
Blink R	1	2	5328614898	5329052628	437730
Saccade L	1	2	5329052628	5329271501	218873	1644.25	108.29	0.00	-5.94	91.78	1732.91	1.00	419.35	43287.71	-43287.26	14240.40
Saccade R	1	2	5329052628	5329231630	179002	1644.25	108.29	1766.73	-11.85	61.72	1732.69	0.89	344.77	43287.71	-37049.42	12326.03
Fixation L	1	3	5329271501	5329799070	527569	0.00	-2.10	0	7	-1	9.22	9.22
Fixation R	1	3	5329291368	5329799070	507702	0.00	-1.88	0	7	-1	11.42	11.42
