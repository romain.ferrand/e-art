[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S11H21112-finaleart-1.idf
Date:	27.03.2019 15:39:31
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S11H21112
Description:	Clement legrand-Duchenne

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	5574457945	# Message: void.jpg
Saccade L	1	1	5574470549	5574530296	59747	1308.14	492.58	1395.12	447.59	1.47	81.38	1.00	24.64	1921.70	-1568.55	911.47
Saccade R	1	1	5574470549	5574530296	59747	1308.14	492.58	1395.12	447.59	1.47	81.38	1.00	24.64	1921.70	-1568.55	911.47
Fixation L	1	1	5574530296	5574968137	437841	1405.04	443.06	19	27	-1	12.05	12.05
Fixation R	1	1	5574530296	5574968137	437841	1405.04	443.06	19	27	-1	13.66	13.66
Blink L	1	1	5574968137	5575625111	656974
Blink R	1	1	5574968137	5575067763	99626
Blink R	1	2	5575087657	5575664980	577323
Saccade L	1	2	5575625111	5575903842	278731	1564.13	273.22	1067.86	433.07	41.16	1656.47	1.00	147.65	940.89	-40325.20	3331.37
Saccade R	1	2	5575664980	5575903842	238862	1451.29	325.65	1067.86	433.07	8.92	93.06	0.00	37.35	940.89	-1093.20	569.19
Fixation L	1	2	5575903842	5576003346	99504	1027.93	438.22	85	11	-1	11.18	11.18
Fixation R	1	2	5575903842	5576003346	99504	1027.93	438.22	85	11	-1	12.87	12.87
Saccade L	1	3	5576003346	5576043092	39746	982.80	440.75	972.84	452.02	0.63	31.07	1.00	15.79	501.93	-296.46	208.54
Saccade R	1	3	5576003346	5576023220	19874	982.80	440.75	972.26	456.54	0.29	31.07	1.00	14.72	20.82	-296.46	110.75
Fixation R	1	3	5576023220	5576321830	298610	939.32	470.46	70	25	-1	12.50	12.50
Fixation L	1	3	5576102844	5576461205	358361	917.20	472.48	55	11	-1	12.10	12.10
Saccade R	1	4	5576321830	5576341713	19883	908.23	469.19	900.16	473.35	0.10	9.18	1.00	4.89	196.04	-78.50	95.69
Fixation R	1	4	5576341713	5576461205	119492	896.65	470.50	9	7	-1	12.21	12.21
