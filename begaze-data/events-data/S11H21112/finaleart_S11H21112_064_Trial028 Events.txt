[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S11H21112-finaleart-1.idf
Date:	27.03.2019 15:38:39
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S11H21112
Description:	Clement legrand-Duchenne

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	4005034638	# Message: void.jpg
Fixation L	1	1	4005047780	4005843356	795576	1370.04	706.90	14	22	-1	13.60	13.60
Fixation R	1	1	4005047780	4005843356	795576	1370.04	706.90	14	22	-1	15.94	15.94
Blink L	1	1	4005843356	4006380479	537123
Blink R	1	1	4005843356	4006380479	537123
Fixation L	1	2	4006380479	4006738452	357973	430.00	465.81	28	61	-1	15.79	15.79
Fixation R	1	2	4006380479	4006738452	357973	430.00	465.81	28	61	-1	16.67	16.67
Saccade L	1	1	4006738452	4006778204	39752	428.99	475.99	230.17	491.16	3.27	252.78	0.50	82.18	5833.64	-5636.86	3926.75
Blink R	1	2	4006738452	4006837946	99494
Fixation L	1	3	4006778204	4006897583	119379	249.25	488.04	40	36	-1	14.40	14.40
Fixation R	1	3	4006837946	4006997062	159116	267.46	505.91	51	36	-1	17.13	17.13
Saccade L	1	2	4006897583	4007016929	119346	270.73	501.80	99.82	502.63	3.58	138.60	1.00	30.02	3289.17	-450.46	815.33
Saccade R	1	1	4006997062	4007016929	19867	236.62	510.60	99.82	502.63	1.32	138.60	1.00	66.25	3289.17	0.00	1644.58
