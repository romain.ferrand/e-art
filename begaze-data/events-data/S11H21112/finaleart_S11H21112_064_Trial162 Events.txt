[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S11H21112-finaleart-1.idf
Date:	27.03.2019 15:39:12
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S11H21112
Description:	Clement legrand-Duchenne

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	5224061752	# Message: void.jpg
Fixation L	1	1	5224074357	5224730695	656338	980.82	594.37	22	58	-1	13.90	13.90
Fixation R	1	1	5224074357	5224730695	656338	980.82	594.37	22	58	-1	16.74	16.74
Saccade L	1	1	5224730695	5224750569	19874	972.19	559.90	971.25	535.84	0.32	24.35	1.00	16.33	117.50	-591.62	242.38
Saccade R	1	1	5224730695	5224750569	19874	972.19	559.90	971.25	535.84	0.32	24.35	1.00	16.33	117.50	-591.62	242.38
Fixation L	1	2	5224750569	5225367194	616625	970.25	539.22	35	63	-1	13.35	13.35
Fixation R	1	2	5224750569	5225367194	616625	970.25	539.22	35	63	-1	16.27	16.27
Saccade L	1	2	5225367194	5225387042	19848	943.12	529.82	934.79	536.46	0.32	23.75	1.00	15.92	966.72	-324.32	519.28
Saccade R	1	2	5225367194	5225387042	19848	943.12	529.82	934.79	536.46	0.32	23.75	1.00	15.92	966.72	-324.32	519.28
Fixation L	1	3	5225387042	5225665523	278481	921.16	494.55	37	60	-1	13.21	13.21
Fixation R	1	3	5225387042	5225665523	278481	921.16	494.55	37	60	-1	16.35	16.35
Saccade L	1	3	5225665523	5225685409	19886	906.51	476.37	902.23	485.96	0.15	13.99	1.00	7.65	275.19	-43.74	169.64
Saccade R	1	3	5225665523	5225685409	19886	906.51	476.37	902.23	485.96	0.15	13.99	1.00	7.65	275.19	-43.74	169.64
Fixation L	1	4	5225685409	5226043387	357978	944.01	491.59	59	28	-1	13.32	13.32
Fixation R	1	4	5225685409	5226043387	357978	944.01	491.59	59	28	-1	16.50	16.50
