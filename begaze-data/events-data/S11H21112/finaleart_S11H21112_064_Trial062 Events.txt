[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S11H21112-finaleart-1.idf
Date:	27.03.2019 15:38:47
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S11H21112
Description:	Clement legrand-Duchenne

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	4275791472	# Message: void.jpg
Fixation L	1	1	4275804203	4276122415	318212	769.69	691.17	10	16	-1	15.97	15.97
Fixation R	1	1	4275804203	4276122415	318212	769.69	691.17	10	16	-1	18.66	18.66
Saccade L	1	1	4276122415	4276142306	19891	767.72	680.75	924.10	689.52	0.93	158.40	1.00	46.54	3901.17	-3871.57	2753.37
Saccade R	1	1	4276122415	4276142306	19891	767.72	680.75	924.10	689.52	0.93	158.40	1.00	46.54	3901.17	-3871.57	2753.37
Fixation L	1	2	4276142306	4276460520	318214	949.81	695.15	31	28	-1	16.42	16.42
Fixation R	1	2	4276142306	4276460520	318214	949.81	695.15	31	28	-1	19.23	19.23
Blink L	1	1	4276460520	4276599767	139247
Blink R	1	1	4276460520	4276599767	139247
Fixation L	1	3	4276599767	4277077137	477370	1291.84	353.78	48	50	-1	17.44	17.44
Fixation R	1	3	4276599767	4277077137	477370	1291.84	353.78	48	50	-1	18.68	18.68
Saccade L	1	2	4277077137	4277116875	39738	1281.09	358.98	1119.44	382.51	1.49	90.34	1.00	37.37	2094.45	-2143.78	1902.18
Saccade R	1	2	4277077137	4277116875	39738	1281.09	358.98	1119.44	382.51	1.49	90.34	1.00	37.37	2094.45	-2143.78	1902.18
Fixation L	1	4	4277116875	4277793234	676359	1114.76	378.18	11	29	-1	18.06	18.06
Fixation R	1	4	4277116875	4277793234	676359	1114.76	378.18	11	29	-1	19.07	19.07
