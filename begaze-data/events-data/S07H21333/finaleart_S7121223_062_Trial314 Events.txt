[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S7121223-finaleart-1.idf
Date:	27.03.2019 15:37:28
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S7121223
Description:	Gregoire pacreau

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	5589322535	# Message: void.jpg
Saccade L	1	1	5589333179	5589392933	59754	445.70	553.54	660.06	842.81	4.68	316.24	1.00	78.35	7828.52	-7799.55	3516.59
Saccade R	1	1	5589333179	5589392933	59754	445.70	553.54	660.06	842.81	4.68	316.24	1.00	78.35	7828.52	-7799.55	3516.59
Fixation L	1	1	5589392933	5590010138	617205	676.46	865.63	34	32	-1	13.62	13.62
Fixation R	1	1	5589392933	5590010138	617205	676.46	865.63	34	32	-1	13.81	13.81
Saccade L	1	2	5590010138	5590089879	79741	671.05	864.00	809.42	652.90	3.66	105.06	0.50	45.90	2243.28	-1715.06	1086.80
Saccade R	1	2	5590010138	5590089879	79741	671.05	864.00	809.42	652.90	3.66	105.06	0.50	45.90	2243.28	-1715.06	1086.80
Fixation L	1	2	5590089879	5590448230	358351	849.72	623.85	58	38	-1	13.97	13.97
Fixation R	1	2	5590089879	5590448230	358351	849.72	623.85	58	38	-1	14.15	14.15
Saccade L	1	3	5590448230	5590468108	19878	868.10	614.99	868.44	608.87	0.11	8.09	1.00	5.48	35.57	-108.16	63.69
Saccade R	1	3	5590448230	5590468108	19878	868.10	614.99	868.44	608.87	0.11	8.09	1.00	5.48	35.57	-108.16	63.69
Fixation L	1	3	5590468108	5591324445	856337	867.54	601.06	11	30	-1	14.90	14.90
Fixation R	1	3	5590468108	5591324445	856337	867.54	601.06	11	30	-1	14.76	14.76
