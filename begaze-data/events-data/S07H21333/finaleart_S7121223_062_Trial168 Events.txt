[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S7121223-finaleart-1.idf
Date:	27.03.2019 15:37:01
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S7121223
Description:	Gregoire pacreau

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	4789701432	# Message: void.jpg
Fixation L	1	1	4789706747	4791239917	1533170	412.47	458.08	47	38	-1	15.81	15.81
Fixation R	1	1	4789706747	4791239917	1533170	412.47	458.08	47	38	-1	15.42	15.42
Saccade L	1	1	4791239917	4791279785	39868	423.21	466.59	779.34	464.01	3.39	191.46	1.00	84.94	4739.49	-4627.60	4059.15
Saccade R	1	1	4791239917	4791279785	39868	423.21	466.59	779.34	464.01	3.39	191.46	1.00	84.94	4739.49	-4627.60	4059.15
Fixation L	1	2	4791279785	4791478918	199133	839.50	446.59	68	27	-1	16.07	16.07
Fixation R	1	2	4791279785	4791478918	199133	839.50	446.59	68	27	-1	15.61	15.61
Saccade L	1	2	4791478918	4791498773	19855	846.66	436.39	846.99	432.49	0.09	6.59	1.00	4.46	20.09	-65.75	47.87
Saccade R	1	2	4791478918	4791498773	19855	846.66	436.39	846.99	432.49	0.09	6.59	1.00	4.46	20.09	-65.75	47.87
Fixation L	1	3	4791498773	4791697886	199113	851.01	432.84	7	6	-1	15.97	15.97
Fixation R	1	3	4791498773	4791697886	199113	851.01	432.84	7	6	-1	15.59	15.59
