[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S7121223-finaleart-1.idf
Date:	27.03.2019 15:37:05
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S7121223
Description:	Gregoire pacreau

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	4856760611	# Message: void.jpg
Fixation L	1	1	4856772131	4856991114	218983	971.50	688.81	6	11	-1	14.30	14.30
Fixation R	1	1	4856772131	4856991114	218983	971.50	688.81	6	11	-1	14.27	14.27
Saccade L	1	1	4856991114	4857031003	39889	971.90	684.29	1265.80	695.64	2.48	150.40	0.50	62.05	3712.28	-3508.43	3593.94
Saccade R	1	1	4856991114	4857031003	39889	971.90	684.29	1265.80	695.64	2.48	150.40	0.50	62.05	3712.28	-3508.43	3593.94
Fixation L	1	2	4857031003	4857548720	517717	1282.16	709.14	20	25	-1	12.89	12.89
Fixation R	1	2	4857031003	4857548720	517717	1282.16	709.14	20	25	-1	12.28	12.28
Saccade L	1	2	4857548720	4857608474	59754	1277.96	697.97	1105.16	690.26	1.98	101.72	0.33	33.19	2458.64	-1657.50	1212.76
Saccade R	1	2	4857548720	4857608474	59754	1277.96	697.97	1105.16	690.26	1.98	101.72	0.33	33.19	2458.64	-1657.50	1212.76
Fixation L	1	3	4857608474	4858763411	1154937	1069.57	667.73	44	37	-1	13.93	13.93
Fixation R	1	3	4857608474	4858763411	1154937	1069.57	667.73	44	37	-1	13.33	13.33
