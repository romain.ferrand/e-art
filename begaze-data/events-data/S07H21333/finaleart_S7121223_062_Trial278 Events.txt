[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S7121223-finaleart-1.idf
Date:	27.03.2019 15:37:22
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S7121223
Description:	Gregoire pacreau

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	5479406510	# Message: void.jpg
Fixation L	1	1	5479424647	5480498697	1074050	560.32	468.41	44	51	-1	15.92	15.92
Fixation R	1	1	5479424647	5480498697	1074050	560.32	468.41	44	51	-1	16.83	16.83
Saccade L	1	1	5480498697	5480518576	19879	572.65	479.02	575.86	485.22	0.07	7.06	1.00	3.53	163.95	-155.26	114.83
Saccade R	1	1	5480498697	5480518576	19879	572.65	479.02	575.86	485.22	0.07	7.06	1.00	3.53	163.95	-155.26	114.83
Fixation L	1	2	5480518576	5481413666	895090	594.95	499.99	44	24	-1	16.37	16.37
Fixation R	1	2	5480518576	5481413666	895090	594.95	499.99	44	24	-1	17.33	17.33
