[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S7121223-finaleart-1.idf
Date:	27.03.2019 15:37:19
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S7121223
Description:	Gregoire pacreau

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	5202973899	# Message: void.jpg
Saccade L	1	1	5202991398	5203031261	39863	1114.67	737.85	1254.62	627.75	2.02	116.10	0.50	50.58	1871.19	-2618.45	1589.36
Saccade R	1	1	5202991398	5203031261	39863	1114.67	737.85	1254.62	627.75	2.02	116.10	0.50	50.58	1871.19	-2618.45	1589.36
Fixation L	1	1	5203031261	5203528857	497596	1253.75	594.21	5	92	-1	12.85	12.85
Fixation R	1	1	5203031261	5203528857	497596	1253.75	594.21	5	92	-1	12.56	12.56
Saccade L	1	2	5203528857	5203548735	19878	1255.53	546.26	1255.01	543.91	0.04	3.23	0.00	2.22	8.75	-53.04	29.29
Saccade R	1	2	5203528857	5203548735	19878	1255.53	546.26	1255.01	543.91	0.04	3.23	0.00	2.22	8.75	-53.04	29.29
Fixation L	1	2	5203548735	5204444431	895696	1239.94	544.53	29	28	-1	14.08	14.08
Fixation R	1	2	5203548735	5204444431	895696	1239.94	544.53	29	28	-1	14.00	14.00
Saccade L	1	3	5204444431	5204464309	19878	1227.37	557.67	1184.97	561.60	0.41	43.07	1.00	20.56	907.17	-956.05	719.01
Saccade R	1	3	5204444431	5204464309	19878	1227.37	557.67	1184.97	561.60	0.41	43.07	1.00	20.56	907.17	-956.05	719.01
Fixation L	1	3	5204464309	5204961901	497592	1149.37	567.05	45	16	-1	15.02	15.02
Fixation R	1	3	5204464309	5204961901	497592	1149.37	567.05	45	16	-1	14.96	14.96
