[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S7121223-finaleart-1.idf
Date:	27.03.2019 15:37:17
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S7121223
Description:	Gregoire pacreau

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	5172443135	# Message: void.jpg
Fixation L	1	1	5172458748	5173712655	1253907	1084.38	711.73	11	38	-1	13.89	13.89
Fixation R	1	1	5172458748	5173712655	1253907	1084.38	711.73	11	38	-1	13.95	13.95
Saccade L	1	1	5173712655	5173752408	39753	1079.01	709.21	991.60	639.14	1.30	76.40	0.50	32.71	1847.74	-1603.92	1521.73
Saccade R	1	1	5173712655	5173752408	39753	1079.01	709.21	991.60	639.14	1.30	76.40	0.50	32.71	1847.74	-1603.92	1521.73
Fixation L	1	2	5173752408	5173991263	238855	950.24	640.08	56	40	-1	13.79	13.79
Fixation R	1	2	5173752408	5173991263	238855	950.24	640.08	56	40	-1	13.67	13.67
Saccade L	1	2	5173991263	5174011260	19997	934.73	622.81	935.19	619.47	0.08	4.67	1.00	3.98	84.86	-31.48	43.21
Saccade R	1	2	5173991263	5174011260	19997	934.73	622.81	935.19	619.47	0.08	4.67	1.00	3.98	84.86	-31.48	43.21
Fixation L	1	3	5174011260	5174429260	418000	933.94	619.74	28	28	-1	13.08	13.08
Fixation R	1	3	5174011260	5174429260	418000	933.94	619.74	28	28	-1	13.09	13.09
