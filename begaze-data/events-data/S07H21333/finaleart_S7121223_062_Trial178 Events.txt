[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S7121223-finaleart-1.idf
Date:	27.03.2019 15:37:03
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S7121223
Description:	Gregoire pacreau

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	4820181330	# Message: void.jpg
Fixation L	1	1	4820192791	4820292433	99642	1087.77	452.79	1	6	-1	11.35	11.35
Fixation R	1	1	4820192791	4820292433	99642	1087.77	452.79	1	6	-1	10.91	10.91
Saccade L	1	1	4820292433	4820312348	19915	1087.19	452.95	928.13	463.78	1.06	161.24	1.00	53.47	3988.25	-3807.28	2979.10
Saccade R	1	1	4820292433	4820312348	19915	1087.19	452.95	928.13	463.78	1.06	161.24	1.00	53.47	3988.25	-3807.28	2979.10
Fixation L	1	2	4820312348	4821029117	716769	873.65	487.20	63	32	-1	11.80	11.80
Fixation R	1	2	4820312348	4821029117	716769	873.65	487.20	63	32	-1	11.70	11.70
Saccade L	1	2	4821029117	4821048999	19882	873.89	496.32	875.52	504.74	0.12	8.67	1.00	6.13	138.00	-178.30	113.75
Saccade R	1	2	4821029117	4821048999	19882	873.89	496.32	875.52	504.74	0.12	8.67	1.00	6.13	138.00	-178.30	113.75
Fixation L	1	3	4821048999	4822164183	1115184	869.07	508.37	44	28	-1	13.61	13.61
Fixation R	1	3	4821048999	4822164183	1115184	869.07	508.37	44	28	-1	13.75	13.75
