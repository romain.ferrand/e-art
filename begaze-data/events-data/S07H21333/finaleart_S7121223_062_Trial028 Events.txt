[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S7121223-finaleart-1.idf
Date:	27.03.2019 15:36:35
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S7121223
Description:	Gregoire pacreau

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	3738539120	# Message: void.jpg
Fixation L	1	1	3738543270	3739279967	736697	558.69	888.51	33	17	-1	12.55	12.55
Fixation R	1	1	3738543270	3739279967	736697	558.69	888.51	33	17	-1	12.26	12.26
Saccade L	1	1	3739279967	3739339728	59761	573.43	893.70	908.23	820.80	3.90	166.91	0.33	65.33	4154.79	-2495.50	2558.00
Saccade R	1	1	3739279967	3739339728	59761	573.43	893.70	908.23	820.80	3.90	166.91	0.33	65.33	4154.79	-2495.50	2558.00
Fixation L	1	2	3739339728	3739797687	457959	915.64	819.10	11	38	-1	15.03	15.03
Fixation R	1	2	3739339728	3739797687	457959	915.64	819.10	11	38	-1	14.78	14.78
Blink L	1	1	3739797687	3740016798	219111
Blink R	1	1	3739797687	3739897310	99623
Blink R	1	2	3739917182	3740016798	99616
Fixation L	1	3	3740016798	3740534524	517726	890.55	503.17	11	70	-1	15.34	15.34
Fixation R	1	3	3740016798	3740534524	517726	890.55	503.17	11	70	-1	17.22	17.22
