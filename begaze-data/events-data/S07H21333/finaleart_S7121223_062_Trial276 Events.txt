[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S7121223-finaleart-1.idf
Date:	27.03.2019 15:37:21
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S7121223
Description:	Gregoire pacreau

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	5473317755	# Message: void.jpg
Fixation L	1	1	5473318719	5473955062	636343	422.63	875.79	10	26	-1	14.12	14.12
Fixation R	1	1	5473318719	5473955062	636343	422.63	875.79	10	26	-1	15.18	15.18
Saccade L	1	1	5473955062	5474034694	79632	423.55	869.00	1114.78	592.46	9.08	286.71	0.50	114.08	7137.54	-4045.00	3785.47
Saccade R	1	1	5473955062	5474034694	79632	423.55	869.00	1114.78	592.46	9.08	286.71	0.50	114.08	7137.54	-4045.00	3785.47
Fixation L	1	2	5474034694	5474392643	357949	1123.43	589.72	53	39	-1	14.25	14.25
Fixation R	1	2	5474034694	5474392643	357949	1123.43	589.72	53	39	-1	14.84	14.84
Saccade L	1	2	5474392643	5474412514	19871	1076.08	565.57	937.53	576.83	1.04	140.59	1.00	52.56	3417.53	-3444.58	2593.65
Saccade R	1	2	5474392643	5474412514	19871	1076.08	565.57	937.53	576.83	1.04	140.59	1.00	52.56	3417.53	-3444.58	2593.65
Fixation L	1	3	5474412514	5475307598	895084	928.78	579.60	14	35	-1	14.55	14.55
Fixation R	1	3	5474412514	5475307598	895084	928.78	579.60	14	35	-1	15.55	15.55
