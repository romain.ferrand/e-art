[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S16F14542-finaleart-1.idf
Date:	27.03.2019 15:47:29
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S16F14542
Description:	Diane Dewez

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	7229756300	# Message: void.jpg
Blink L	1	1	7229761079	7229999805	238726
Blink R	1	1	7229761079	7229999805	238726
Fixation L	1	1	7229999805	7230318040	318235	781.76	467.85	14	22	-1	14.89	14.89
Fixation R	1	1	7229999805	7230318040	318235	781.76	467.85	14	22	-1	14.86	14.86
Blink L	1	2	7230318040	7230576535	258495
Blink R	1	2	7230318040	7230397536	79496
Blink R	1	3	7230417543	7230576535	158992
Fixation L	1	2	7230576535	7231471608	895073	844.66	596.56	26	70	-1	13.69	13.69
Fixation R	1	2	7230576535	7231471608	895073	844.66	596.56	26	70	-1	13.04	13.04
Saccade L	1	1	7231471608	7231491490	19882	840.54	584.54	885.67	565.78	0.38	49.44	1.00	19.31	1178.47	-1038.75	918.67
Saccade R	1	1	7231471608	7231491490	19882	840.54	584.54	885.67	565.78	0.38	49.44	1.00	19.31	1178.47	-1038.75	918.67
Fixation L	1	3	7231491490	7231750099	258609	903.69	538.23	32	34	-1	12.65	12.65
Fixation R	1	3	7231491490	7231750099	258609	903.69	538.23	32	34	-1	12.25	12.25
