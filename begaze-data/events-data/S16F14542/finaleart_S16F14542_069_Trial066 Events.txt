[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S16F14542-finaleart-1.idf
Date:	27.03.2019 15:46:28
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S16F14542
Description:	Diane Dewez

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	6515762113	# Message: void.jpg
Fixation L	1	1	6515773186	6516210771	437585	579.94	344.85	6	37	-1	13.41	13.41
Fixation R	1	1	6515773186	6516210771	437585	579.94	344.85	6	37	-1	14.82	14.82
Blink L	1	1	6516210771	6516767751	556980
Blink R	1	1	6516210771	6516767751	556980
Fixation L	1	2	6516767751	6517682706	914955	769.47	364.72	26	49	-1	14.48	14.48
Fixation R	1	2	6516767751	6517682706	914955	769.47	364.72	26	49	-1	15.01	15.01
Saccade L	1	1	6517682706	6517762209	79503	771.29	340.63	886.48	442.64	2.46	91.84	0.25	30.93	2272.00	-1977.17	1428.86
Saccade R	1	1	6517682706	6517762209	79503	771.29	340.63	886.48	442.64	2.46	91.84	0.25	30.93	2272.00	-1977.17	1428.86
