[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S16F14542-finaleart-1.idf
Date:	27.03.2019 15:47:27
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S16F14542
Description:	Diane Dewez

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	7217550232	# Message: void.jpg
Fixation L	1	1	7217568543	7217707768	139225	355.66	602.93	4	6	-1	15.62	15.62
Fixation R	1	1	7217568543	7217707768	139225	355.66	602.93	4	6	-1	15.79	15.79
Saccade L	1	1	7217707768	7217747537	39769	355.46	605.53	560.90	592.81	1.80	106.49	1.00	45.32	2634.96	-2518.88	2452.38
Saccade R	1	1	7217707768	7217747537	39769	355.46	605.53	560.90	592.81	1.80	106.49	1.00	45.32	2634.96	-2518.88	2452.38
Fixation L	1	2	7217747537	7218205004	457467	571.44	576.59	23	59	-1	15.68	15.68
Fixation R	1	2	7217747537	7218205004	457467	571.44	576.59	23	59	-1	15.66	15.66
Saccade L	1	2	7218205004	7218244747	39743	577.68	533.16	580.46	464.37	1.67	103.22	1.00	41.95	18723.47	-489.40	5444.98
Saccade R	1	2	7218205004	7218244747	39743	577.68	533.16	580.46	464.37	1.67	103.22	1.00	41.95	18723.47	-489.40	5444.98
Blink L	1	1	7218244747	7218741971	497224
Blink R	1	1	7218324365	7218741971	417606
Fixation L	1	3	7218741971	7219537563	795592	818.11	524.97	48	28	-1	13.10	13.10
Saccade R	1	3	7218741971	7218761857	19886	855.01	521.80	850.98	515.72	0.23	23.48	1.00	11.80	245.21	-402.78	243.57
Fixation R	1	3	7218781841	7219537563	755722	816.32	525.29	40	28	-1	12.46	12.46
