[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S16F14542-finaleart-1.idf
Date:	27.03.2019 15:46:39
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S16F14542
Description:	Diane Dewez

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	6607336663	# Message: void.jpg
Saccade L	1	1	6607346932	6607446417	99485	997.22	389.21	760.42	565.51	5.38	250.13	1.00	54.10	6107.17	-5425.60	1870.81
Saccade R	1	1	6607346932	6607446417	99485	997.22	389.21	760.42	565.51	5.38	250.13	1.00	54.10	6107.17	-5425.60	1870.81
Fixation L	1	1	6607446417	6608023143	576726	721.19	602.68	46	53	-1	14.50	14.50
Fixation R	1	1	6607446417	6608023143	576726	721.19	602.68	46	53	-1	14.40	14.40
Blink L	1	1	6608023143	6608401130	377987
Blink R	1	1	6608023143	6608401130	377987
Fixation L	1	2	6608401130	6609335964	934834	952.31	509.14	34	40	-1	12.75	12.75
Fixation R	1	2	6608401130	6609335964	934834	952.31	509.14	34	40	-1	12.63	12.63
