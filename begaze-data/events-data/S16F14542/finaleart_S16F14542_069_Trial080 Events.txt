[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S16F14542-finaleart-1.idf
Date:	27.03.2019 15:46:33
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S16F14542
Description:	Diane Dewez

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	6558525443	# Message: void.jpg
Fixation L	1	1	6558536765	6558656126	119361	1282.95	969.60	17	38	-1	13.20	13.20
Fixation R	1	1	6558536765	6558656126	119361	1282.95	969.60	17	38	-1	12.12	12.12
Blink L	1	1	6558656126	6558894878	238752
Blink R	1	1	6558656126	6558894878	238752
Fixation L	1	2	6558894878	6560505915	1611037	900.69	599.92	19	37	-1	13.05	13.05
Fixation R	1	2	6558894878	6560505915	1611037	900.69	599.92	19	37	-1	12.46	12.46
