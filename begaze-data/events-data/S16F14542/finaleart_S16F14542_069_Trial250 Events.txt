[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S16F14542-finaleart-1.idf
Date:	27.03.2019 15:47:34
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S16F14542
Description:	Diane Dewez

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	7272466264	# Message: void.jpg
Fixation L	1	1	7272483657	7273000754	517097	928.63	571.62	41	14	-1	13.15	13.15
Fixation R	1	1	7272483657	7273000754	517097	928.63	571.62	41	14	-1	12.92	12.92
Blink L	1	1	7273000754	7274023578	1022824
Blink R	1	1	7273000754	7274023578	1022824
Fixation L	1	2	7274023578	7274162822	139244	955.50	522.55	4	14	-1	10.12	10.12
Fixation R	1	2	7274023578	7274182694	159116	955.65	521.84	4	14	-1	9.89	9.89
Fixation L	1	3	7274182694	7274461189	278495	954.00	515.01	47	21	-1	9.55	9.55
Fixation R	1	3	7274202570	7274461189	258619	953.80	514.93	47	21	-1	8.58	8.58
