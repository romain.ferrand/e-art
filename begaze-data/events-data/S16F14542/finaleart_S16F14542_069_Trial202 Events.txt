[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S16F14542-finaleart-1.idf
Date:	27.03.2019 15:47:16
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S16F14542
Description:	Diane Dewez

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	7068097835	# Message: void.jpg
Fixation L	1	1	7068108416	7068307285	198869	825.20	841.65	14	11	-1	16.03	16.03
Fixation R	1	1	7068108416	7068307285	198869	825.20	841.65	14	11	-1	16.38	16.38
Blink L	1	1	7068307285	7068426647	119362
Blink R	1	1	7068307285	7068426647	119362
Fixation L	1	2	7068426647	7069142622	715975	1128.53	608.77	13	25	-1	15.71	15.71
Fixation R	1	2	7068426647	7069142622	715975	1128.53	608.77	13	25	-1	15.90	15.90
Saccade L	1	1	7069142622	7069182494	39872	1121.82	599.68	790.98	594.89	2.79	193.17	0.50	70.01	4807.14	-4633.19	4038.20
Saccade R	1	1	7069142622	7069182494	39872	1121.82	599.68	790.98	594.89	2.79	193.17	0.50	70.01	4807.14	-4633.19	4038.20
Fixation L	1	3	7069182494	7070077444	894950	772.77	563.74	27	49	-1	12.85	12.85
Fixation R	1	3	7069182494	7070077444	894950	772.77	563.74	27	49	-1	13.26	13.26
