[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S16F14542-finaleart-1.idf
Date:	27.03.2019 15:46:50
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S16F14542
Description:	Diane Dewez

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	6812585342	# Message: void.jpg
Fixation L	1	1	6812588506	6812688004	99498	1530.14	976.00	11	14	-1	16.36	16.36
Fixation R	1	1	6812588506	6812688004	99498	1530.14	976.00	11	14	-1	14.15	14.15
Saccade L	1	1	6812688004	6812707877	19873	1525.34	980.05	1381.48	864.60	1.21	186.54	1.00	60.90	4600.05	-4506.36	3417.63
Saccade R	1	1	6812688004	6812707877	19873	1525.34	980.05	1381.48	864.60	1.21	186.54	1.00	60.90	4600.05	-4506.36	3417.63
Fixation L	1	2	6812707877	6813085839	377962	1385.45	812.61	20	59	-1	15.73	15.73
Fixation R	1	2	6812707877	6813085839	377962	1385.45	812.61	20	59	-1	13.75	13.75
Blink L	1	1	6813085839	6813583073	497234
Blink R	1	1	6813085839	6813583073	497234
Fixation L	1	3	6813583073	6814319033	735960	894.95	459.49	38	42	-1	12.83	12.83
Fixation R	1	3	6813583073	6814319033	735960	894.95	459.49	38	42	-1	12.88	12.88
Saccade L	1	2	6814319033	6814338910	19877	870.64	488.40	692.68	491.63	1.07	180.00	1.00	53.71	4358.58	-4010.79	2845.93
Saccade R	1	2	6814319033	6814338910	19877	870.64	488.40	692.68	491.63	1.07	180.00	1.00	53.71	4358.58	-4010.79	2845.93
Fixation L	1	4	6814338910	6814577527	238617	659.83	506.51	46	21	-1	10.99	10.99
Fixation R	1	4	6814338910	6814577527	238617	659.83	506.51	46	21	-1	11.16	11.16
