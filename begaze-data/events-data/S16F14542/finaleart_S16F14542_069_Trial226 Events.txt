[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S16F14542-finaleart-1.idf
Date:	27.03.2019 15:47:25
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S16F14542
Description:	Diane Dewez

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	7199247601	# Message: void.jpg
Saccade L	1	1	7199249788	7199289530	39742	1106.43	491.64	1103.81	488.96	0.35	29.54	1.00	8.79	910.51	0.00	398.28
Fixation R	1	1	7199249788	7199945996	696208	1050.12	498.68	69	20	-1	10.43	10.43
Blink L	1	1	7199289530	7199389025	99495
Fixation L	1	1	7199389025	7199945996	556971	1043.55	500.43	11	16	-1	11.81	11.81
Blink L	1	2	7199945996	7200105124	159128
Blink R	1	1	7199945996	7200105124	159128
Blink L	1	3	7200124995	7200542589	417594
Blink R	1	2	7200124995	7200542589	417594
Fixation L	1	2	7200542589	7200840944	298355	1161.77	617.15	41	39	-1	14.25	14.25
Fixation R	1	2	7200542589	7200840944	298355	1161.77	617.15	41	39	-1	14.41	14.41
Saccade L	1	2	7200840944	7200900692	59748	1143.15	632.65	878.88	490.82	3.40	192.31	0.33	56.96	4753.96	-3947.85	2764.41
Saccade R	1	1	7200840944	7200900692	59748	1143.15	632.65	878.88	490.82	3.40	192.31	0.33	56.96	4753.96	-3947.85	2764.41
Fixation L	1	3	7200900692	7201238808	338116	869.92	438.46	25	73	-1	11.47	11.47
Fixation R	1	3	7200900692	7201238808	338116	869.92	438.46	25	73	-1	11.85	11.85
