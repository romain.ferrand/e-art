[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S16F14542-finaleart-1.idf
Date:	27.03.2019 15:47:39
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S16F14542
Description:	Diane Dewez

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	7315211964	# Message: void.jpg
Fixation L	1	1	7315228516	7315427348	198832	1054.06	490.92	7	40	-1	16.96	16.96
Fixation R	1	1	7315228516	7315427348	198832	1054.06	490.92	7	40	-1	16.13	16.13
Blink L	1	1	7315427348	7315606354	179006
Blink R	1	1	7315427348	7315606354	179006
Fixation L	1	2	7315606354	7315864966	258612	1101.63	538.07	16	44	-1	17.88	17.88
Fixation R	1	2	7315606354	7315864966	258612	1101.63	538.07	16	44	-1	18.71	18.71
Blink L	1	2	7315864966	7316421802	556836
Blink R	1	2	7315864966	7316421802	556836
Fixation L	1	3	7316421802	7316998657	576855	1002.38	564.55	36	51	-1	13.93	13.93
Fixation R	1	3	7316421802	7316998657	576855	1002.38	564.55	36	51	-1	13.83	13.83
Saccade L	1	1	7316998657	7317018532	19875	993.35	526.38	844.48	447.26	1.21	170.50	1.00	61.09	3972.81	-3799.41	2658.31
Saccade R	1	1	7316998657	7317018532	19875	993.35	526.38	844.48	447.26	1.21	170.50	1.00	61.09	3972.81	-3799.41	2658.31
Fixation L	1	4	7317018532	7317197544	179012	826.29	434.66	24	20	-1	11.72	11.72
Fixation R	1	4	7317018532	7317197544	179012	826.29	434.66	24	20	-1	11.40	11.40
