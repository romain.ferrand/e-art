[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S4120113-finaleart-1.idf
Date:	27.03.2019 15:34:23
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S4120113
Description:	Vidal Attias

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	6392007768	# Message: void.jpg
Fixation L	1	1	6392023655	6392580614	556959	1107.01	682.68	14	67	-1	11.47	11.47
Fixation R	1	1	6392023655	6392580614	556959	1107.01	682.68	14	67	-1	12.98	12.98
Blink L	1	1	6392580614	6392839123	258509
Blink R	1	1	6392580614	6392839123	258509
Fixation L	1	2	6392839123	6393475566	636443	1158.75	313.81	24	62	-1	14.10	14.10
Fixation R	1	2	6392839123	6393475566	636443	1158.75	313.81	24	62	-1	15.06	15.06
Saccade L	1	1	6393475566	6393495446	19880	1166.12	334.46	1165.48	392.05	0.66	58.26	1.00	33.10	1115.01	-1264.16	1134.05
Saccade R	1	1	6393475566	6393495446	19880	1166.12	334.46	1165.48	392.05	0.66	58.26	1.00	33.10	1115.01	-1264.16	1134.05
Fixation L	1	3	6393495446	6393793794	298348	1156.51	363.92	17	50	-1	14.52	14.52
Fixation R	1	3	6393495446	6393793794	298348	1156.51	363.92	17	50	-1	15.67	15.67
Saccade L	1	2	6393793794	6393833662	39868	1149.85	375.12	1049.20	470.66	1.37	90.62	1.00	34.28	2186.89	-1989.50	1663.60
Saccade R	1	2	6393793794	6393833662	39868	1149.85	375.12	1049.20	470.66	1.37	90.62	1.00	34.28	2186.89	-1989.50	1663.60
Fixation L	1	4	6393833662	6393992784	159122	1052.38	483.89	12	19	-1	14.94	14.94
Fixation R	1	4	6393833662	6393992784	159122	1052.38	483.89	12	19	-1	15.75	15.75
