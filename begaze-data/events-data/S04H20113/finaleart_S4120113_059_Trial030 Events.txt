[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S4120113-finaleart-1.idf
Date:	27.03.2019 15:34:20
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S4120113
Description:	Vidal Attias

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	6318466855	# Message: void.jpg
Fixation L	1	1	6318471229	6318908816	437587	805.07	679.38	9	59	-1	12.51	12.51
Fixation R	1	1	6318471229	6318908816	437587	805.07	679.38	9	59	-1	12.56	12.56
Saccade L	1	1	6318908816	6318928692	19876	809.47	708.08	811.54	741.21	0.41	33.58	1.00	20.51	225.08	-296.65	178.27
Saccade R	1	1	6318908816	6318928692	19876	809.47	708.08	811.54	741.21	0.41	33.58	1.00	20.51	225.08	-296.65	178.27
Fixation L	1	2	6318928692	6320122112	1193420	810.68	731.93	25	70	-1	13.31	13.31
Fixation R	1	2	6318928692	6320122112	1193420	810.68	731.93	25	70	-1	13.31	13.31
Saccade L	1	2	6320122112	6320141997	19885	824.91	721.72	856.08	669.10	0.63	61.87	1.00	31.55	1414.24	-1189.80	1119.68
Saccade R	1	2	6320122112	6320141997	19885	824.91	721.72	856.08	669.10	0.63	61.87	1.00	31.55	1414.24	-1189.80	1119.68
Fixation L	1	3	6320141997	6320420479	278482	901.68	679.27	59	38	-1	14.41	14.41
Fixation R	1	3	6320141997	6320420479	278482	901.68	679.27	59	38	-1	14.31	14.31
Saccade L	1	3	6320420479	6320460232	39753	914.57	658.35	941.36	595.05	0.85	47.20	1.00	21.47	982.97	0.00	443.46
Saccade R	1	3	6320420479	6320460232	39753	914.57	658.35	941.36	595.05	0.85	47.20	1.00	21.47	982.97	0.00	443.46
