[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S4120113-finaleart-1.idf
Date:	27.03.2019 15:34:37
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S4120113
Description:	Vidal Attias

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	7263394674	# Message: void.jpg
Fixation L	1	1	7263405802	7263704155	298353	513.75	659.44	11	28	-1	12.82	12.82
Fixation R	1	1	7263405802	7263704155	298353	513.75	659.44	11	28	-1	12.70	12.70
Saccade L	1	1	7263704155	7263724026	19871	514.18	643.26	517.61	537.53	0.77	107.01	1.00	38.91	2560.46	-2412.36	1766.48
Saccade R	1	1	7263704155	7263724026	19871	514.18	643.26	517.61	537.53	0.77	107.01	1.00	38.91	2560.46	-2412.36	1766.48
Fixation L	1	2	7263724026	7263883143	159117	528.87	558.88	18	38	-1	12.96	12.96
Fixation R	1	2	7263724026	7263883143	159117	528.87	558.88	18	38	-1	13.09	13.09
Blink L	1	1	7263883143	7264082004	198861
Blink R	1	1	7263883143	7264082004	198861
Fixation L	1	3	7264082004	7265116315	1034311	1169.03	401.49	66	32	-1	13.87	13.87
Fixation R	1	3	7264082004	7265116315	1034311	1169.03	401.49	66	32	-1	13.88	13.88
Saccade L	1	2	7265116315	7265136204	19889	1147.03	385.08	1150.28	375.57	0.15	14.57	0.00	7.72	182.80	-282.32	187.07
Saccade R	1	2	7265116315	7265136204	19889	1147.03	385.08	1150.28	375.57	0.15	14.57	0.00	7.72	182.80	-282.32	187.07
Fixation L	1	4	7265136204	7265454413	318209	1151.86	413.13	10	65	-1	14.51	14.51
Fixation R	1	4	7265136204	7265454413	318209	1151.86	413.13	10	65	-1	14.51	14.51
