[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S4120113-finaleart-1.idf
Date:	27.03.2019 15:34:18
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S4120113
Description:	Vidal Attias

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	6257048876	# Message: void.jpg
Fixation L	1	1	6257051583	6257429572	377989	473.22	815.09	30	67	-1	10.74	10.74
Fixation R	1	1	6257051583	6257429572	377989	473.22	815.09	30	67	-1	10.92	10.92
Saccade L	1	1	6257429572	6257449411	19839	452.61	825.50	449.56	822.17	0.16	13.76	1.00	7.84	-55.57	-229.82	138.22
Saccade R	1	1	6257429572	6257449411	19839	452.61	825.50	449.56	822.17	0.16	13.76	1.00	7.84	-55.57	-229.82	138.22
Fixation L	1	2	6257449411	6257588641	139230	444.88	820.71	8	8	-1	10.70	10.70
Fixation R	1	2	6257449411	6257588641	139230	444.88	820.71	8	8	-1	11.07	11.07
Saccade L	1	2	6257588641	6257727894	139253	441.53	821.13	1191.10	385.71	13.85	249.97	0.29	99.45	6127.12	-4603.51	2600.13
Saccade R	1	2	6257588641	6257727894	139253	441.53	821.13	1191.10	385.71	13.85	249.97	0.29	99.45	6127.12	-4603.51	2600.13
Fixation L	1	3	6257727894	6258225116	497222	1169.11	364.20	36	62	-1	12.46	12.46
Fixation R	1	3	6257727894	6258225116	497222	1169.11	364.20	36	62	-1	11.77	11.77
Blink L	1	1	6258225116	6258503589	278473
Blink R	1	1	6258225116	6258503589	278473
Fixation L	1	4	6258503589	6259040569	536980	1066.25	575.06	14	47	-1	13.34	13.34
Fixation R	1	4	6258503589	6259040569	536980	1066.25	575.06	14	47	-1	13.04	13.04
