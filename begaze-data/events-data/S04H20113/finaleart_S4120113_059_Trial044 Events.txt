[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S4120113-finaleart-1.idf
Date:	27.03.2019 15:34:22
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S4120113
Description:	Vidal Attias

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	6361328400	# Message: void.jpg
Fixation L	1	1	6361333779	6361492885	159106	571.00	645.37	9	16	-1	10.14	10.14
Fixation R	1	1	6361333779	6361492885	159106	571.00	645.37	9	16	-1	11.70	11.70
Saccade L	1	1	6361492885	6361532626	39741	572.02	651.55	917.59	632.16	3.01	212.75	0.50	75.85	5176.36	-5027.65	4151.91
Saccade R	1	1	6361492885	6361532626	39741	572.02	651.55	917.59	632.16	3.01	212.75	0.50	75.85	5176.36	-5027.65	4151.91
Fixation L	1	2	6361532626	6362825528	1292902	935.17	598.85	24	67	-1	11.23	11.23
Fixation R	1	2	6361532626	6362825528	1292902	935.17	598.85	24	67	-1	12.40	12.40
Saccade L	1	2	6362825528	6362845441	19913	936.50	572.79	936.26	559.74	0.81	63.87	0.00	40.88	329.02	-823.07	580.31
Saccade R	1	2	6362825528	6362845441	19913	936.50	572.79	936.26	559.74	0.81	63.87	0.00	40.88	329.02	-823.07	580.31
Fixation L	1	3	6362845441	6363322761	477320	938.53	589.75	17	52	-1	12.38	12.38
Fixation R	1	3	6362845441	6363322761	477320	938.53	589.75	17	52	-1	13.58	13.58
