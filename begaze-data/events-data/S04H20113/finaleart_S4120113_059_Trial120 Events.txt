[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S4120113-finaleart-1.idf
Date:	27.03.2019 15:34:31
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S4120113
Description:	Vidal Attias

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	6940077193	# Message: void.jpg
Fixation L	1	1	6940085074	6940303921	218847	886.62	635.01	36	25	-1	12.10	12.10
Fixation R	1	1	6940085074	6940303921	218847	886.62	635.01	36	25	-1	13.28	13.28
Saccade L	1	1	6940303921	6940323796	19875	860.79	624.46	669.38	417.82	1.78	284.71	1.00	89.48	7004.80	-6946.89	4687.34
Saccade R	1	1	6940303921	6940323796	19875	860.79	624.46	669.38	417.82	1.78	284.71	1.00	89.48	7004.80	-6946.89	4687.34
Fixation L	1	2	6940323796	6940661902	338106	655.49	444.64	18	53	-1	12.07	12.07
Fixation R	1	2	6940323796	6940661902	338106	655.49	444.64	18	53	-1	13.41	13.41
Blink L	1	1	6940661902	6940920547	258645
Blink R	1	1	6940661902	6940920547	258645
Fixation L	1	3	6940920547	6941457485	536938	840.01	448.05	19	75	-1	12.33	12.33
Fixation R	1	3	6940920547	6941457485	536938	840.01	448.05	19	75	-1	11.81	11.81
Saccade L	1	2	6941457485	6941477357	19872	850.75	408.93	855.03	407.77	0.24	24.16	0.00	12.18	-76.78	-338.86	180.38
Saccade R	1	2	6941457485	6941477357	19872	850.75	408.93	855.03	407.77	0.24	24.16	0.00	12.18	-76.78	-338.86	180.38
Fixation L	1	4	6941477357	6942074068	596711	845.74	454.02	17	75	-1	12.28	12.28
Fixation R	1	4	6941477357	6942074068	596711	845.74	454.02	17	75	-1	11.82	11.82
