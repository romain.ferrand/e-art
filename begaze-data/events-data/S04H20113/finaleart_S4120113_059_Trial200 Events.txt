[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S4120113-finaleart-1.idf
Date:	27.03.2019 15:34:40
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S4120113
Description:	Vidal Attias

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	7325592207	# Message: void.jpg
Fixation L	1	1	7325601043	7325780172	179129	747.32	241.62	31	57	-1	11.92	11.92
Fixation R	1	1	7325601043	7325780172	179129	747.32	241.62	31	57	-1	12.33	12.33
Saccade L	1	1	7325780172	7325800028	19856	730.34	214.53	725.17	204.41	0.27	19.32	1.00	13.66	263.68	34.21	146.16
Saccade R	1	1	7325780172	7325800028	19856	730.34	214.53	725.17	204.41	0.27	19.32	1.00	13.66	263.68	34.21	146.16
Fixation L	1	2	7325800028	7326018758	218730	711.63	181.04	24	39	-1	12.22	12.22
Fixation R	1	2	7325800028	7326018758	218730	711.63	181.04	24	39	-1	12.81	12.81
Blink L	1	1	7326018758	7326911585	892827
Blink R	1	1	7326018758	7326911585	892827
Fixation L	1	3	7326911585	7327010945	99360	1142.44	522.15	7	16	-1	12.68	12.68
Fixation R	1	3	7326911585	7327010945	99360	1142.44	522.15	7	16	-1	12.28	12.28
Fixation L	1	4	7327070715	7327408812	338097	1135.10	503.83	6	67	-1	10.13	10.13
Fixation R	1	4	7327070715	7327170190	99475	1134.95	517.26	4	26	-1	9.96	9.96
Fixation R	1	5	7327229818	7327587802	357984	1133.64	513.14	15	57	-1	11.70	11.70
Saccade L	1	2	7327408812	7327428681	19869	1132.54	496.72	1123.17	550.70	0.51	55.42	1.00	25.48	1283.06	-837.08	1011.82
Fixation L	1	5	7327428681	7327587802	159121	1131.18	527.78	11	37	-1	10.13	10.13
