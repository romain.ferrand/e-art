[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S4120113-finaleart-1.idf
Date:	27.03.2019 15:34:34
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S4120113
Description:	Vidal Attias

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	7038462186	# Message: void.jpg
Fixation L	1	1	7038479874	7038917467	437593	845.38	749.30	14	36	-1	12.90	12.90
Fixation R	1	1	7038479874	7038917467	437593	845.38	749.30	14	36	-1	11.47	11.47
Blink L	1	1	7038917467	7039295329	377862
Blink R	1	1	7038917467	7039056729	139262
Blink R	1	2	7039076604	7039275450	198846
Fixation R	1	2	7039275450	7040110915	835465	911.77	526.68	25	73	-1	13.32	13.32
Fixation L	1	2	7039295329	7040110915	815586	911.83	527.42	25	67	-1	12.05	12.05
Saccade L	1	1	7040110915	7040130769	19854	916.40	568.67	915.83	577.76	0.19	17.51	0.00	9.40	30.69	-366.04	154.84
Saccade R	1	1	7040110915	7040130769	19854	916.40	568.67	915.83	577.76	0.19	17.51	0.00	9.40	30.69	-366.04	154.84
Fixation L	1	3	7040130769	7040449004	318235	909.53	567.15	10	62	-1	11.99	11.99
Fixation R	1	3	7040130769	7040449004	318235	909.53	567.15	10	62	-1	13.40	13.40
