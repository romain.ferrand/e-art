[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S14F27123-finaleart-1.idf
Date:	27.03.2019 15:43:32
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S14F27123
Description:	Anne-flore Perrin

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	31284418937	# Message: void.jpg
Fixation L	1	1	31284425994	31284704451	278457	1316.22	991.35	15	18	-1	13.36	13.36
Fixation R	1	1	31284425994	31284704451	278457	1316.22	991.35	15	18	-1	13.48	13.48
Saccade L	1	1	31284704451	31284724336	19885	1309.79	1000.38	1462.34	1000.04	0.90	154.29	1.00	45.02	3822.99	-3628.48	2661.46
Saccade R	1	1	31284704451	31284724336	19885	1309.79	1000.38	1462.34	1000.04	0.90	154.29	1.00	45.02	3822.99	-3628.48	2661.46
Fixation L	1	2	31284724336	31285022695	298359	1494.18	992.11	39	17	-1	13.60	13.60
Fixation R	1	2	31284724336	31285022695	298359	1494.18	992.11	39	17	-1	12.93	12.93
Blink L	1	1	31285022695	31285500043	477348
Blink R	1	1	31285022695	31285500043	477348
Fixation L	1	3	31285500043	31286355265	855222	402.71	522.04	24	75	-1	12.97	12.97
Fixation R	1	3	31285500043	31286355265	855222	402.71	522.04	24	75	-1	13.51	13.51
Saccade L	1	2	31286355265	31286415012	59747	388.93	565.87	391.35	582.08	0.31	11.89	0.67	5.15	166.30	-6.77	68.63
Saccade R	1	2	31286355265	31286415012	59747	388.93	565.87	391.35	582.08	0.31	11.89	0.67	5.15	166.30	-6.77	68.63
