[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S14F27123-finaleart-1.idf
Date:	27.03.2019 15:43:23
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S14F27123
Description:	Anne-flore Perrin

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	30927084131	# Message: void.jpg
Fixation L	1	1	30927093747	30927252882	159135	1433.56	1002.79	29	28	-1	21.22	21.22
Fixation R	1	1	30927093747	30927252882	159135	1433.56	1002.79	29	28	-1	12.25	12.25
Blink L	1	1	30927252882	30927392092	139210
Blink R	1	1	30927252882	30927392092	139210
Saccade L	1	1	30927392092	30927431853	39761	1595.28	449.99	1470.34	349.59	2.09	96.42	1.00	52.60	2133.86	-1658.64	1519.11
Saccade R	1	1	30927392092	30927431853	39761	1595.28	449.99	1470.34	349.59	2.09	96.42	1.00	52.60	2133.86	-1658.64	1519.11
Fixation L	1	2	30927431853	30927551215	119362	1497.35	317.80	39	44	-1	12.21	12.21
Fixation R	1	2	30927431853	30927551215	119362	1497.35	317.80	39	44	-1	12.11	12.11
Blink L	1	2	30927551215	30927968837	417622
Blink R	1	2	30927551215	30927968837	417622
Fixation L	1	3	30928048450	30929082796	1034346	758.52	666.99	24	66	-1	12.30	12.30
Fixation R	1	3	30928048450	30929082796	1034346	758.52	666.99	24	66	-1	13.45	13.17
