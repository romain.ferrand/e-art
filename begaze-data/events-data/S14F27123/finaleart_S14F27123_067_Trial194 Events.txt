[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S14F27123-finaleart-1.idf
Date:	27.03.2019 15:43:42
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S14F27123
Description:	Anne-flore Perrin

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	31375994557	# Message: void.jpg
Fixation L	1	1	31376059740	31376417725	357985	1368.92	833.32	10	50	-1	15.36	15.36
Fixation R	1	1	31376059740	31376417725	357985	1368.92	833.32	10	50	-1	11.64	11.64
Blink L	1	1	31376417725	31377410186	992461
Blink R	1	1	31376417725	31377410186	992461
Fixation L	1	2	31377410186	31377987053	576867	827.58	590.91	19	63	-1	9.70	9.70
Fixation R	1	2	31377410186	31377987053	576867	827.58	590.91	19	63	-1	10.74	10.74
