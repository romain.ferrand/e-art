[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S14F27123-finaleart-1.idf
Date:	27.03.2019 15:43:17
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S14F27123
Description:	Anne-flore Perrin

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	30872077015	# Message: void.jpg
Saccade L	1	1	30872077640	30872137266	59626	775.50	212.23	1207.85	223.63	5.42	278.96	0.67	90.89	6974.08	-6566.65	3177.76
Saccade R	1	1	30872077640	30872137266	59626	775.50	212.23	1207.85	223.63	5.42	278.96	0.67	90.89	6974.08	-6566.65	3177.76
Fixation L	1	1	30872137266	30872257021	119755	1219.73	243.81	18	41	-1	10.92	10.92
Fixation R	1	1	30872137266	30872257021	119755	1219.73	243.81	18	41	-1	11.72	11.72
Saccade L	1	2	30872257021	30872296509	39488	1222.03	264.91	1599.78	274.67	3.41	289.98	0.49	86.28	6968.67	-6970.40	4846.09
Saccade R	1	2	30872257021	30872296509	39488	1222.03	264.91	1599.78	274.67	3.41	289.98	0.49	86.28	6968.67	-6970.40	4846.09
Fixation L	1	2	30872296509	30872515230	218721	1586.27	295.63	30	36	-1	11.29	11.29
Fixation R	1	2	30872296509	30872515230	218721	1586.27	295.63	30	36	-1	11.66	11.66
Blink L	1	1	30872515230	30873271073	755843
Blink R	1	1	30872515230	30873271073	755843
Fixation L	1	3	30873271073	30874066674	795601	825.62	596.90	17	37	-1	12.40	12.40
Fixation R	1	3	30873271073	30874066674	795601	825.62	596.90	17	37	-1	12.89	12.89
