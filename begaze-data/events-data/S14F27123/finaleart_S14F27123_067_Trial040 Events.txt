[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S14F27123-finaleart-1.idf
Date:	27.03.2019 15:42:54
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S14F27123
Description:	Anne-flore Perrin

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	30386085839	# Message: void.jpg
Fixation L	1	1	30386132769	30386272006	139237	1789.45	984.92	25	19	-1	23.80	23.80
Fixation R	1	1	30386132769	30386272006	139237	1789.45	984.92	25	19	-1	13.07	13.07
Saccade L	1	1	30386272006	30386291866	19860	1784.89	995.61	1665.59	787.41	1.39	242.60	1.00	69.86	6046.95	-4983.75	3859.38
Saccade R	1	1	30386272006	30386291866	19860	1784.89	995.61	1665.59	787.41	1.39	242.60	1.00	69.86	6046.95	-4983.75	3859.38
Fixation L	1	2	30386291866	30386550480	258614	1657.84	788.60	41	27	-1	15.04	15.04
Fixation R	1	2	30386291866	30386550480	258614	1657.84	788.60	41	27	-1	13.14	13.14
Blink L	1	1	30386550480	30386948348	397868
Blink R	1	1	30386550480	30386948348	397868
Fixation L	1	3	30386968215	30387584820	616605	798.43	600.02	19	80	-1	13.91	13.91
Fixation R	1	3	30386968215	30387584820	616605	798.43	600.02	19	80	-1	14.52	14.52
Saccade L	1	2	30387584820	30387604693	19873	804.22	609.54	810.91	623.42	0.18	15.59	1.00	8.92	303.25	-315.17	226.73
Saccade R	1	2	30387584820	30387604693	19873	804.22	609.54	810.91	623.42	0.18	15.59	1.00	8.92	303.25	-315.17	226.73
Fixation L	1	4	30387604693	30388082043	477350	814.23	615.00	5	41	-1	13.13	13.13
Fixation R	1	4	30387604693	30388082043	477350	814.23	615.00	5	41	-1	13.60	13.60
