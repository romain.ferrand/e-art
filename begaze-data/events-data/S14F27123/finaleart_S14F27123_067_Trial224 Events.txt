[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S14F27123-finaleart-1.idf
Date:	27.03.2019 15:43:51
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S14F27123
Description:	Anne-flore Perrin

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	31496461433	# Message: void.jpg
Fixation L	1	1	31496471783	31496630880	159097	719.84	496.46	4	13	-1	13.06	13.06
Fixation R	1	1	31496471783	31496630880	159097	719.84	496.46	4	13	-1	13.23	13.23
Saccade L	1	1	31496630880	31496670753	39873	717.84	501.13	1123.08	608.10	3.46	267.95	0.50	86.71	6666.97	-6531.01	5171.05
Saccade R	1	1	31496630880	31496670753	39873	717.84	501.13	1123.08	608.10	3.46	267.95	0.50	86.71	6666.97	-6531.01	5171.05
Fixation L	1	2	31496670753	31496909358	238605	1125.47	626.30	6	40	-1	12.92	12.92
Fixation R	1	2	31496670753	31496909358	238605	1125.47	626.30	6	40	-1	12.99	12.99
Blink L	1	1	31496909358	31497525962	616604
Blink R	1	1	31496909358	31497525962	616604
Fixation L	1	3	31497525962	31497625453	99491	808.89	737.13	7	6	-1	12.44	12.44
Fixation R	1	3	31497525962	31497625453	99491	808.89	737.13	7	6	-1	12.83	12.83
Blink L	1	2	31497625453	31497943711	318258
Blink R	1	2	31497625453	31497943711	318258
