[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S14F27123-finaleart-1.idf
Date:	27.03.2019 15:43:44
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S14F27123
Description:	Anne-flore Perrin

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	31400419832	# Message: void.jpg
Saccade L	1	1	31400423068	31400522565	99497	335.66	812.40	555.05	1052.69	5.22	255.25	0.80	52.45	6346.35	-5884.73	2228.85
Saccade R	1	1	31400423068	31400522565	99497	335.66	812.40	555.05	1052.69	5.22	255.25	0.80	52.45	6346.35	-5884.73	2228.85
Fixation L	1	1	31400522565	31400820906	298341	589.42	1027.48	52	41	-1	17.08	17.08
Fixation R	1	1	31400522565	31400820906	298341	589.42	1027.48	52	41	-1	12.36	12.36
Blink L	1	1	31400820906	31400960154	139248
Blink R	1	1	31400820906	31400960154	139248
Blink L	1	2	31400980033	31401973862	993829
Blink R	1	2	31400980033	31401973862	993829
Fixation L	1	2	31401973862	31402192727	218865	796.02	654.52	13	29	-1	10.51	10.51
Fixation R	1	2	31401973862	31402212599	238737	796.64	653.02	16	36	-1	11.48	11.48
Blink L	1	3	31402192727	31402292101	99374
Saccade R	1	2	31402212599	31402272218	59619	804.06	634.95	813.61	653.04	8.44	362.49	0.67	141.65	8261.81	-8791.54	5687.30
Fixation R	1	3	31402272218	31402411471	139253	802.35	653.32	17	47	-1	10.59	10.59
Fixation L	1	3	31402292101	31402411471	119370	800.74	653.36	8	47	-1	11.47	11.47
