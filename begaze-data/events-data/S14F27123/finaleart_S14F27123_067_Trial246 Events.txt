[BeGaze]
Converted from:	C:\Documents and Settings\iViewX\Desktop\EXPERIMENTATIONS\Experiments\results\finaleart\S14F27123-finaleart-1.idf
Date:	27.03.2019 15:43:58
Version:	BeGaze 2.5.77
Sample Rate:	50
Subject:	S14F27123
Description:	Anne-flore Perrin

Table Header for Fixations:
Event Type	Trial	Number	Start	End	Duration	Location X	Location Y	Dispersion X	Dispersion Y	Plane	Avg. Pupil Size X	Avg. Pupil Size Y

Table Header for Saccades:
Event Type	Trial	Number	Start	End	Duration	Start Loc.X	Start Loc.Y	End Loc.X	End Loc.Y	Amplitude	Peak Speed	Peak Speed At	Average Speed	Peak Accel.	Peak Decel.	Average Accel.

Table Header for Blinks:
Event Type	Trial	Number	Start	End	Duration

Table Header for User Events:
Event Type	Trial	Number	Start	Description

UserEvent	1	1	31563613252	# Message: void.jpg
Fixation L	1	1	31563620849	31564078316	457467	1309.58	418.76	23	22	-1	10.28	10.28
Fixation R	1	1	31563620849	31564078316	457467	1309.58	418.76	23	22	-1	10.52	10.52
Blink L	1	1	31564078316	31564197579	119263
Blink R	1	1	31564078316	31564197579	119263
Blink L	1	2	31564217441	31564734664	517223
Blink R	1	2	31564217441	31564734664	517223
Saccade L	1	1	31564734664	31564754555	19891	793.07	737.34	787.29	734.75	10.80	1078.17	1.00	543.02	26640.28	-26794.11	17864.84
Saccade R	1	1	31564734664	31564754555	19891	793.07	737.34	787.29	734.75	10.80	1078.17	1.00	543.02	26640.28	-26794.11	17864.84
Blink L	1	3	31564754555	31565251763	497208
Blink R	1	3	31564754555	31565251763	497208
