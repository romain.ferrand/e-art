import os
import cv2

IMG_ROW = 1600
IMG_COL = 900


GRIS_VOULU = [100,100,100]

def resize(path):
    path_old = path
    path = 'images/'+path
    print(path)
    img = cv2.imread(path, cv2.IMREAD_COLOR)
    border_v = 0
    border_h = 0
    if (IMG_COL/IMG_ROW) >= (img.shape[0]/img.shape[1]):
        border_v = int((((IMG_COL/IMG_ROW)*img.shape[1])-img.shape[0])/2)
    else:
        border_h = int((((IMG_ROW/IMG_COL)*img.shape[0])-img.shape[1])/2)
    img = cv2.copyMakeBorder(img, border_v, border_v, border_h, border_h, cv2.BORDER_CONSTANT, None, GRIS_VOULU)
    img = cv2.resize(img, (IMG_ROW, IMG_COL))

    cv2.imwrite('resized/'+path_old, img)

    return 0

if __name__ == '__main__':
    files = os.listdir('../images')
    files.remove(".DS_Store")
    #files.remove('.DS_Store')
    for i, f in enumerate(files):
        print(i)
        resize(f)
