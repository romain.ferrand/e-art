Projet E-ART :

L'attention visuelle humaine nous permet de concentrer nos ressources de traitement sur des zones spécifiques 
de l'environnement visuelle. L'attention impliquant des mouvements oculaires est principalement influencée 
par des mécanismes bottom-up (inconscient et très rapide) et des mécanismes top-down (conscient et lent).  
Dans l'équipe PERCEPT, nous cherchons à déterminer les zones d'une image qui attirent le regard. 
Des modèles de saillance sont ainsi réalisés [Bannier et al., 2018] et évalués par rapport à des enregistrements oculaires [Le Meur & Baccino, 2013]. 
Actuellement, les modèles proposés par l'équipe sont performants sur des images naturelles 
et des pages webs. Nous souhaitons étendre les modèles existants à des oeuvres d'art.

Le projet consiste donc à étudier et à modéliser le déploiement attentionnel d'observateurs sur des œuvres d'art.  
Le projet se déroulera en différentes étapes:
(1) réalisation d'une base de données d'images représentant des oeuvres d'art. On s'oriente sur des tableaux de grands peintres de la renaissance;
(2) réalisation d'une étude oculométrique, consistant à enregistrer le parcours visuel d'un panel d'observateurs regardant les oeuvres choisies. 
Deux équipements sont disponibles pour réaliser ces tests (SMI RED50 et lunettes Tobi);
(3) dépouillement et étude statistique des résultats;
(4) adaptation d'un modèle d'apprentissage profond (deep learning) de l'équipe [Bannier et al., 2018] pour prédire les zones regardées par les observateurs

Les résultats obtenus seront à comparer aux études existantes comme [Balbi et al., 2016; Quian Quiroga & Pedreira, 2011; Walker et al., 2017; Villani et al., 2015].
